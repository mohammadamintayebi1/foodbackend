//add social
    $("#tab1 .addVsocial").click(function() {
        let newsocial = social_list('new')
        $("#tab1 .vsocial").append(newsocial);
    });

    $("body").on("click", "#tab1 .vsocial i.la-times", function() {
        $(this).parent(".col-xl-6").remove();
    })

    //add Educational Background
    $(".addEducationalBackground").click(function() {
        let newEducation = education_list('new')
        $("#tab2 .education").append(newEducation);
    });

    $("body").on("click", "#tab2 .education i.la-times", function() {
        $(this).parent(".row").remove();
    })

    //add Work Experience
    $(".addWorkExperience").click(function() {
        let newWorkExperience = work_experience_list('new')
        $("#tab3 .work_experience").append(newWorkExperience);
    });

    $("body").on("click", "#tab3 .work_experience i.la-times", function() {
        $(this).parent(".row").remove();
    })

    //add Langs
    $(".addLangs").click(function() {
        let newLang = lang_list('new')
        $("#tab4 .langs").append(newLang);
    });

    $("body").on("click", "#tab4 .langs i.la-times", function() {
        $(this).parent(".row").remove();
    })

    //add Skills
    $(".addSkills").click(function() {
        let newSkill = skills_list('new')
        $("#tab4 .skills").append(newSkill);
    });

    $("body").on("click", "#tab4 .skills i.la-times", function() {
        $(this).parent(".col-xl-6").remove();
    })

    //add Projects
    $(".addProjects").click(function() {
        let newProject = projects_list('new')
        $("#tab5 .projects").append(newProject);
    });

    $("body").on("click", "#tab5 .projects i.la-times", function() {
        $(this).parent(".row").remove();
    })

    //add Researches
    $(".addResearches").click(function() {
        let newResearche = researches_list('new')
        $("#tab6 .researches").append(newResearche);
    });

    $("body").on("click", "#tab6 .researches i.la-times", function() {
        $(this).parent(".row").remove();
    })


function social_list(data) {
        if (data == 'new') {
            return `<div class="col-xl-6 bg-white shadow-sm p-3">
                                                        <i class="la la-times"></i>
                                                        <div class="row p-3">
                                                            <div class="col-xl-4">
                                                                <label class="form-control-label">شبکه
                                                                    اجتماعی</label>
                                                                <select id="country" class="custom-select form-control">
                                                                    <option value="" disabled="">
                                                                    </option>
                                                                    <option value="LinkedIn">لینکداین</option>
                                                                    <option value="Twitter">توییتر</option>
                                                                    <option value="Facebook">فیسبوک</option>
                                                                    <option value="Instagram">اینستاگرام</option>
                                                                    <option value="Telegram">تلگرام</option>
                                                                    <option value="Github">گیت&zwnj;هاب</option>
                                                                    <option value="Dribbble">دریبل</option>
                                                                    <option value="WhatsApp">واتساپ</option>
                                                                    <option value="Skype">اسکایپ</option>
                                                                    <option value="Youtube">یوتیوب</option>
                                                                    <option value="Stackoverflow">StackOverflow
                                                                    </option>
                                                                    <option value="ResearchGate">ResearchGate
                                                                    </option>
                                                                    <option value="Figma">فیگما</option>
                                                                    <option value="Pinterest">Pinterest</option>
                                                                    <option value="Gitlab">گیت&zwnj;لب</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xl-8">
                                                                <label class="form-control-label">آی دی
                                                                    مرتبط</label>
                                                                <input type="text" class="form-control" dir="ltr" />
                                                            </div>
                                                        </div>

                                                    </div>`
        } else {
            return `<div class="col-xl-6 bg-white shadow-sm p-3"> <i class="la la-times"></i> <div class="row p-3"> <div class="col-xl-4"> <label class="form-control-label">شبکه اجتماعی</label><select id="country" class="custom-select form-control">
        <option value="" disabled=""></option>
        <option value="LinkedIn"${data.social == 'LinkedIn' ? ' selected' : ''}>لینکداین</option>
        <option value="Twitter"${data.social == 'Twitter' ? ' selected' : ''}>توییتر</option>
        <option value="Facebook"${data.social == 'Facebook' ? ' selected' : ''}>فیسبوک</option>
        <option value="Instagram"${data.social == 'Instagram' ? ' selected' : ''}>اینستاگرام</option>
        <option value="Telegram"${data.social == 'Telegram' ? ' selected' : ''}>تلگرام</option>
        <option value="Github"${data.social == 'Github' ? ' selected' : ''}>گیت&zwnj;هاب</option>
        <option value="Dribbble"${data.social == 'Dribbble' ? ' selected' : ''}>دریبل</option>
        <option value="WhatsApp"${data.social == 'WhatsApp' ? ' selected' : ''}>واتساپ</option>
        <option value="Skype"${data.social == 'Skype' ? ' selected' : ''}>اسکایپ</option>
        <option value="Youtube"${data.social == 'Youtube' ? ' selected' : ''}>یوتیوب</option>
        <option value="Stackoverflow"${data.social == 'Stackoverflow' ? ' selected' : ''}>StackOverflow </option>
        <option value="ResearchGate"${data.social == 'ResearchGate' ? ' selected' : ''}>ResearchGate </option>
        <option value="Figma"${data.social == 'Figma' ? ' selected' : ''}>فیگما</option>
        <option value="Pinterest"${data.social == 'Pinterest' ? ' selected' : ''}>Pinterest</option>
        <option value="Gitlab"${data.social == 'Gitlab' ? ' selected' : ''}>گیت&zwnj;لب</option>
      </select></div><div class="col-xl-8"> <label class="form-control-label">آی دی مرتبط</label> <input type="text" class="form-control" value="${data.link == null ? '' : data.link}" dir="ltr" /> </div></div></div>`
        }
    }

    function education_list(data) {
        if (data == 'new') {
            return `<div class="row bg-white mb-3 p-3" style="position:relative">
                                                        <i class="la la-times"></i>
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">مقطع</label>
                                                                    <select id="grade" class="custom-select form-control">
                                                                        <option value="" selected=""></option>
                                                                        <option value="UnderGraduate">زیر دیپلم </option>
                                                                        <option value="Diploma">دیپلم</option>
                                                                        <option value="Associate">کاردانی</option>
                                                                        <option value="Bachelor">کارشناسی</option>
                                                                        <option value="Master">کارشناسی ارشد</option>
                                                                        <option value="Doctorate">دکتری</option>
                                                                        <option value="Postdoctoral">فوق دکتری</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">رشته تحصیلی</label>
                                                                    <input type="text" class="form-control" id="FieldofStudy" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">گرایش/تخصص</label>
                                                                    <input type="text" class="form-control" id="orientation" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">نوع موسسه</label>
                                                                    <select id="Typeofinstitution" class="custom-select form-control">
                                                                        <option value="" selected=""></option>
                                                                        <option value="Dolati">دولتی</option>
                                                                        <option value="Tizhooshan">تیزهوشان</option>
                                                                        <option value="GheireEntefayi">غیرانتفاعی </option>
                                                                        <option value="NemooneDowlati">نمونه دولتی </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">دانشگاه</label>
                                                                    <input list="brow" class="form-control" style="background: none" id="university">
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">معدل</label>
                                                                    <input type="text" class="form-control" id="gpa" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">کشور</label>
                                                                    <input type="text" class="form-control" id="county" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">استان</label>
                                                                    <input type="text" class="form-control" id="state" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">شهر</label>
                                                                    <input type="text" class="form-control" id="city" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">سال ورود</label>
                                                                    <select id="enteringYear" class="custom-select form-control">
                                                                        <option value="" selected=""></option>
                                                                        <option value="1401">1401</option>
                                                                        <option value="1400">1400</option>
                                                                        <option value="1399">1399</option>
                                                                        <option value="1398">1398</option>
                                                                        <option value="1397">1397</option>
                                                                        <option value="1396">1396</option>
                                                                        <option value="1395">1395</option>
                                                                        <option value="1394">1394</option>
                                                                        <option value="1393">1393</option>
                                                                        <option value="1392">1392</option>
                                                                        <option value="1391">1391</option>
                                                                        <option value="1390">1390</option>
                                                                        <option value="1389">1389</option>
                                                                        <option value="1388">1388</option>
                                                                        <option value="1387">1387</option>
                                                                        <option value="1386">1386</option>
                                                                        <option value="1385">1385</option>
                                                                        <option value="1384">1384</option>
                                                                        <option value="1383">1383</option>
                                                                        <option value="1382">1382</option>
                                                                        <option value="1381">1381</option>
                                                                        <option value="1380">1380</option>
                                                                        <option value="1379">1379</option>
                                                                        <option value="1378">1378</option>
                                                                        <option value="1377">1377</option>
                                                                        <option value="1376">1376</option>
                                                                        <option value="1375">1375</option>
                                                                        <option value="1374">1374</option>
                                                                        <option value="1373">1373</option>
                                                                        <option value="1372">1372</option>
                                                                        <option value="1371">1371</option>
                                                                        <option value="1370">1370</option>
                                                                        <option value="1369">1369</option>
                                                                        <option value="1368">1368</option>
                                                                        <option value="1367">1367</option>
                                                                        <option value="1366">1366</option>
                                                                        <option value="1365">1365</option>
                                                                        <option value="1364">1364</option>
                                                                        <option value="1363">1363</option>
                                                                        <option value="1362">1362</option>
                                                                        <option value="1361">1361</option>
                                                                        <option value="1360">1360</option>
                                                                        <option value="1359">1359</option>
                                                                        <option value="1358">1358</option>
                                                                        <option value="1357">1357</option>
                                                                        <option value="1356">1356</option>
                                                                        <option value="1355">1355</option>
                                                                        <option value="1354">1354</option>
                                                                        <option value="1353">1353</option>
                                                                        <option value="1352">1352</option>
                                                                        <option value="1351">1351</option>
                                                                        <option value="1350">1350</option>
                                                                        <option value="1349">1349</option>
                                                                        <option value="1348">1348</option>
                                                                        <option value="1347">1347</option>
                                                                        <option value="1346">1346</option>
                                                                        <option value="1345">1345</option>
                                                                        <option value="1344">1344</option>
                                                                        <option value="1343">1343</option>
                                                                        <option value="1342">1342</option>
                                                                        <option value="1341">1341</option>
                                                                        <option value="1340">1340</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">فراغت از تحصیل</label>
                                                                    <input type="text" class="form-control" id="graduationYear" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">در حال تحصیل</label>
                                                                    <input type="radio" class="form-control" id="studying" name="studying" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>`
        } else {
            return `<div class="row bg-white mb-3 p-3" style="position:relative">
  <i class="la la-times"></i>
  <div class="col-xl-12">
    <div class="row">
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">مقطع</label>
        <select id="grade" class="custom-select form-control">
          <option value="" selected=""></option>
          <option value="UnderGraduate"${data.grade == 'UnderGraduate' ? ' selected' : ''}>زیر دیپلم </option>
          <option value="Diploma"${data.grade == 'Diploma' ? ' selected' : ''}>دیپلم</option>
          <option value="Associate"${data.grade == 'Associate' ? ' selected' : ''}>کاردانی</option>
          <option value="Bachelor"${data.grade == 'Bachelor' ? ' selected' : ''}>کارشناسی</option>
          <option value="Master"${data.grade == 'Master' ? ' selected' : ''}>کارشناسی ارشد</option>
          <option value="Doctorate"${data.grade == 'Doctorate' ? ' selected' : ''}>دکتری</option>
          <option value="Postdoctoral"${data.grade == 'Postdoctoral' ? ' selected' : ''}>فوق دکتری</option>
        </select>
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">رشته تحصیلی</label>
        <input type="text" class="form-control" id="FieldofStudy" value="${data.FieldofStudy == null ? '' : data.FieldofStudy}" />
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">گرایش/تخصص</label>
        <input type="text" class="form-control" id="orientation" value="${data.orientation == null ? '' : data.orientation}" />
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">نوع موسسه</label>
        <select id="Typeofinstitution" class="custom-select form-control">
          <option value="" selected=""></option>
          <option value="Dolati"${data.Typeofinstitution == 'Dolati' ? ' selected' : ''}>دولتی</option>
          <option value="Tizhooshan"${data.Typeofinstitution == 'Tizhooshan' ? ' selected' : ''}>تیزهوشان</option>
          <option value="GheireEntefayi"${data.Typeofinstitution == 'GheireEntefayi' ? ' selected' : ''}>غیرانتفاعی </option>
          <option value="NemooneDowlati"${data.Typeofinstitution == 'NemooneDowlati' ? ' selected' : ''}>نمونه دولتی </option>
        </select>
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">دانشگاه</label>
        <input list="brow" class="form-control" style="background: none" id="university" value="${data.university == null ? '' : data.university}">
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">معدل</label>
        <input type="text" class="form-control" id="gpa" value="${data.gpa == null ? '' : data.gpa}" />
      </div>
    </div>
    <div class="row">
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">کشور</label>
        <input type="text" class="form-control" id="county" value="${data.county == null ? '' : data.county}" />
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">استان</label>
        <input type="text" class="form-control" id="state" value="${data.state == null ? '' : data.state}" />
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">شهر</label>
        <input type="text" class="form-control" id="city" value="${data.city == null ? '' : data.city}" />
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">سال ورود</label>
        <select id="enteringYear" class="custom-select form-control">
            <option value="" selected=""></option>
            <option value="1401"${data.enteringYear == "1401" ? " selected" : ""}>1401</option>
            <option value="1400"${data.enteringYear == "1400" ? " selected" : ""}>1400</option>
            <option value="1399"${data.enteringYear == "1399" ? " selected" : ""}>1399</option>
            <option value="1398"${data.enteringYear == "1398" ? " selected" : ""}>1398</option>
            <option value="1397"${data.enteringYear == "1397" ? " selected" : ""}>1397</option>
            <option value="1396"${data.enteringYear == "1396" ? " selected" : ""}>1396</option>
            <option value="1395"${data.enteringYear == "1395" ? " selected" : ""}>1395</option>
            <option value="1394"${data.enteringYear == "1394" ? " selected" : ""}>1394</option>
            <option value="1393"${data.enteringYear == "1393" ? " selected" : ""}>1393</option>
            <option value="1392"${data.enteringYear == "1392" ? " selected" : ""}>1392</option>
            <option value="1391"${data.enteringYear == "1391" ? " selected" : ""}>1391</option>
            <option value="1390"${data.enteringYear == "1390" ? " selected" : ""}>1390</option>
            <option value="1389"${data.enteringYear == "1389" ? " selected" : ""}>1389</option>
            <option value="1388"${data.enteringYear == "1388" ? " selected" : ""}>1388</option>
            <option value="1387"${data.enteringYear == "1387" ? " selected" : ""}>1387</option>
            <option value="1386"${data.enteringYear == "1386" ? " selected" : ""}>1386</option>
            <option value="1385"${data.enteringYear == "1385" ? " selected" : ""}>1385</option>
            <option value="1384"${data.enteringYear == "1384" ? " selected" : ""}>1384</option>
            <option value="1383"${data.enteringYear == "1383" ? " selected" : ""}>1383</option>
            <option value="1382"${data.enteringYear == "1382" ? " selected" : ""}>1382</option>
            <option value="1381"${data.enteringYear == "1381" ? " selected" : ""}>1381</option>
            <option value="1380"${data.enteringYear == "1380" ? " selected" : ""}>1380</option>
            <option value="1379"${data.enteringYear == "1379" ? " selected" : ""}>1379</option>
            <option value="1378"${data.enteringYear == "1378" ? " selected" : ""}>1378</option>
            <option value="1377"${data.enteringYear == "1377" ? " selected" : ""}>1377</option>
            <option value="1376"${data.enteringYear == "1376" ? " selected" : ""}>1376</option>
            <option value="1375"${data.enteringYear == "1375" ? " selected" : ""}>1375</option>
            <option value="1374"${data.enteringYear == "1374" ? " selected" : ""}>1374</option>
            <option value="1373"${data.enteringYear == "1373" ? " selected" : ""}>1373</option>
            <option value="1372"${data.enteringYear == "1372" ? " selected" : ""}>1372</option>
            <option value="1371"${data.enteringYear == "1371" ? " selected" : ""}>1371</option>
            <option value="1370"${data.enteringYear == "1370" ? " selected" : ""}>1370</option>
            <option value="1369"${data.enteringYear == "1369" ? " selected" : ""}>1369</option>
            <option value="1368"${data.enteringYear == "1368" ? " selected" : ""}>1368</option>
            <option value="1367"${data.enteringYear == "1367" ? " selected" : ""}>1367</option>
            <option value="1366"${data.enteringYear == "1366" ? " selected" : ""}>1366</option>
            <option value="1365"${data.enteringYear == "1365" ? " selected" : ""}>1365</option>
            <option value="1364"${data.enteringYear == "1364" ? " selected" : ""}>1364</option>
            <option value="1363"${data.enteringYear == "1363" ? " selected" : ""}>1363</option>
            <option value="1362"${data.enteringYear == "1362" ? " selected" : ""}>1362</option>
            <option value="1361"${data.enteringYear == "1361" ? " selected" : ""}>1361</option>
            <option value="1360"${data.enteringYear == "1360" ? " selected" : ""}>1360</option>
            <option value="1359"${data.enteringYear == "1359" ? " selected" : ""}>1359</option>
            <option value="1358"${data.enteringYear == "1358" ? " selected" : ""}>1358</option>
            <option value="1357"${data.enteringYear == "1357" ? " selected" : ""}>1357</option>
            <option value="1356"${data.enteringYear == "1356" ? " selected" : ""}>1356</option>
            <option value="1355"${data.enteringYear == "1355" ? " selected" : ""}>1355</option>
            <option value="1354"${data.enteringYear == "1354" ? " selected" : ""}>1354</option>
            <option value="1353"${data.enteringYear == "1353" ? " selected" : ""}>1353</option>
            <option value="1352"${data.enteringYear == "1352" ? " selected" : ""}>1352</option>
            <option value="1351"${data.enteringYear == "1351" ? " selected" : ""}>1351</option>
            <option value="1350"${data.enteringYear == "1350" ? " selected" : ""}>1350</option>
            <option value="1349"${data.enteringYear == "1349" ? " selected" : ""}>1349</option>
            <option value="1348"${data.enteringYear == "1348" ? " selected" : ""}>1348</option>
            <option value="1347"${data.enteringYear == "1347" ? " selected" : ""}>1347</option>
            <option value="1346"${data.enteringYear == "1346" ? " selected" : ""}>1346</option>
            <option value="1345"${data.enteringYear == "1345" ? " selected" : ""}>1345</option>
            <option value="1344"${data.enteringYear == "1344" ? " selected" : ""}>1344</option>
            <option value="1343"${data.enteringYear == "1343" ? " selected" : ""}>1343</option>
            <option value="1342"${data.enteringYear == "1342" ? " selected" : ""}>1342</option>
            <option value="1341"${data.enteringYear == "1341" ? " selected" : ""}>1341</option>
            <option value="1340"${data.enteringYear == "1340" ? " selected" : ""}>1340</option>
        </select>
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">فراغت از تحصیل</label>
        <input type="text" class="form-control" id="graduationYear" value="${data.graduationYear == null ? '' : data.graduationYear}" />
      </div>
      <div class="col-xl-2 mb-3">
        <label class="form-control-label">در حال تحصیل</label>
        <input type="radio" class="form-control" id="studying" name="studying"${data.studying == "true" ? ' checked' : ''} />
      </div>
    </div>
  </div>
</div>`
        }
    }

    function work_experience_list(data) {
        if (data == 'new') {
            return `<div class="row bg-white mb-3 p-3" style="position:relative">
                                                    <i class="la la-times"></i>
                                                    <div class="col-xl-12">
                                                        <div class="row">
                                                            <div class="col-xl-3 mb-3">
                                                                <label class="form-control-label">سِمت شغلی
                                                                    مربوطه</label>
                                                                <input type="text" class="form-control" id="job_position" />
                                                            </div>
                                                            <div class="col-xl-3 mb-3">
                                                                <label class="form-control-label">عنوان محل
                                                                    کار</label>
                                                                <input type="text" class="form-control" id="workplace_title" />
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">کشور</label>
                                                                <input type="text" class="form-control" id="country" />
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">استان</label>
                                                                <input type="text" class="form-control" id="state" />
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">شهر</label>
                                                                <input type="text" class="form-control" id="city" />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">شروع</label>
                                                                <select id="start_month" class="custom-select form-control">
                                                                    <option value="" selected="">
                                                                    </option>
                                                                    <option value="1">فروردین</option>
                                                                    <option value="2">اردیبهشت</option>
                                                                    <option value="3">خرداد</option>
                                                                    <option value="4">تیر</option>
                                                                    <option value="5">مرداد</option>
                                                                    <option value="6">شهریور</option>
                                                                    <option value="7">مهر</option>
                                                                    <option value="8">آبان</option>
                                                                    <option value="9">آذر</option>
                                                                    <option value="10">دی</option>
                                                                    <option value="11">بهمن</option>
                                                                    <option value="12">اسفند</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">&nbsp;</label>
                                                                <select id="start_year" class="custom-select form-control">
                                                                    <option value="" selected="">
                                                                    </option>
                                                                    <option value="1401">1401</option>
                                                                    <option value="1400">1400</option>
                                                                    <option value="1399">1399</option>
                                                                    <option value="1398">1398</option>
                                                                    <option value="1397">1397</option>
                                                                    <option value="1396">1396</option>
                                                                    <option value="1395">1395</option>
                                                                    <option value="1394">1394</option>
                                                                    <option value="1393">1393</option>
                                                                    <option value="1392">1392</option>
                                                                    <option value="1391">1391</option>
                                                                    <option value="1390">1390</option>
                                                                    <option value="1389">1389</option>
                                                                    <option value="1388">1388</option>
                                                                    <option value="1387">1387</option>
                                                                    <option value="1386">1386</option>
                                                                    <option value="1385">1385</option>
                                                                    <option value="1384">1384</option>
                                                                    <option value="1383">1383</option>
                                                                    <option value="1382">1382</option>
                                                                    <option value="1381">1381</option>
                                                                    <option value="1380">1380</option>
                                                                    <option value="1379">1379</option>
                                                                    <option value="1378">1378</option>
                                                                    <option value="1377">1377</option>
                                                                    <option value="1376">1376</option>
                                                                    <option value="1375">1375</option>
                                                                    <option value="1374">1374</option>
                                                                    <option value="1373">1373</option>
                                                                    <option value="1372">1372</option>
                                                                    <option value="1371">1371</option>
                                                                    <option value="1370">1370</option>
                                                                    <option value="1369">1369</option>
                                                                    <option value="1368">1368</option>
                                                                    <option value="1367">1367</option>
                                                                    <option value="1366">1366</option>
                                                                    <option value="1365">1365</option>
                                                                    <option value="1364">1364</option>
                                                                    <option value="1363">1363</option>
                                                                    <option value="1362">1362</option>
                                                                    <option value="1361">1361</option>
                                                                    <option value="1360">1360</option>
                                                                    <option value="1359">1359</option>
                                                                    <option value="1358">1358</option>
                                                                    <option value="1357">1357</option>
                                                                    <option value="1356">1356</option>
                                                                    <option value="1355">1355</option>
                                                                    <option value="1354">1354</option>
                                                                    <option value="1353">1353</option>
                                                                    <option value="1352">1352</option>
                                                                    <option value="1351">1351</option>
                                                                    <option value="1350">1350</option>
                                                                    <option value="1349">1349</option>
                                                                    <option value="1348">1348</option>
                                                                    <option value="1347">1347</option>
                                                                    <option value="1346">1346</option>
                                                                    <option value="1345">1345</option>
                                                                    <option value="1344">1344</option>
                                                                    <option value="1343">1343</option>
                                                                    <option value="1342">1342</option>
                                                                    <option value="1341">1341</option>
                                                                    <option value="1340">1340</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">اتمام</label>
                                                                <select id="end_month" class="custom-select form-control">
                                                                    <option value="" selected="">
                                                                    </option>
                                                                    <option value="1">فروردین</option>
                                                                    <option value="2">اردیبهشت</option>
                                                                    <option value="3">خرداد</option>
                                                                    <option value="4">تیر</option>
                                                                    <option value="5">مرداد</option>
                                                                    <option value="6">شهریور</option>
                                                                    <option value="7">مهر</option>
                                                                    <option value="8">آبان</option>
                                                                    <option value="9">آذر</option>
                                                                    <option value="10">دی</option>
                                                                    <option value="11">بهمن</option>
                                                                    <option value="12">اسفند</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">&nbsp;</label>
                                                                <select id="end_year" class="custom-select form-control">
                                                                    <option value="" selected="">
                                                                    </option>
                                                                    <option value="1401">1401</option>
                                                                    <option value="1400">1400</option>
                                                                    <option value="1399">1399</option>
                                                                    <option value="1398">1398</option>
                                                                    <option value="1397">1397</option>
                                                                    <option value="1396">1396</option>
                                                                    <option value="1395">1395</option>
                                                                    <option value="1394">1394</option>
                                                                    <option value="1393">1393</option>
                                                                    <option value="1392">1392</option>
                                                                    <option value="1391">1391</option>
                                                                    <option value="1390">1390</option>
                                                                    <option value="1389">1389</option>
                                                                    <option value="1388">1388</option>
                                                                    <option value="1387">1387</option>
                                                                    <option value="1386">1386</option>
                                                                    <option value="1385">1385</option>
                                                                    <option value="1384">1384</option>
                                                                    <option value="1383">1383</option>
                                                                    <option value="1382">1382</option>
                                                                    <option value="1381">1381</option>
                                                                    <option value="1380">1380</option>
                                                                    <option value="1379">1379</option>
                                                                    <option value="1378">1378</option>
                                                                    <option value="1377">1377</option>
                                                                    <option value="1376">1376</option>
                                                                    <option value="1375">1375</option>
                                                                    <option value="1374">1374</option>
                                                                    <option value="1373">1373</option>
                                                                    <option value="1372">1372</option>
                                                                    <option value="1371">1371</option>
                                                                    <option value="1370">1370</option>
                                                                    <option value="1369">1369</option>
                                                                    <option value="1368">1368</option>
                                                                    <option value="1367">1367</option>
                                                                    <option value="1366">1366</option>
                                                                    <option value="1365">1365</option>
                                                                    <option value="1364">1364</option>
                                                                    <option value="1363">1363</option>
                                                                    <option value="1362">1362</option>
                                                                    <option value="1361">1361</option>
                                                                    <option value="1360">1360</option>
                                                                    <option value="1359">1359</option>
                                                                    <option value="1358">1358</option>
                                                                    <option value="1357">1357</option>
                                                                    <option value="1356">1356</option>
                                                                    <option value="1355">1355</option>
                                                                    <option value="1354">1354</option>
                                                                    <option value="1353">1353</option>
                                                                    <option value="1352">1352</option>
                                                                    <option value="1351">1351</option>
                                                                    <option value="1350">1350</option>
                                                                    <option value="1349">1349</option>
                                                                    <option value="1348">1348</option>
                                                                    <option value="1347">1347</option>
                                                                    <option value="1346">1346</option>
                                                                    <option value="1345">1345</option>
                                                                    <option value="1344">1344</option>
                                                                    <option value="1343">1343</option>
                                                                    <option value="1342">1342</option>
                                                                    <option value="1341">1341</option>
                                                                    <option value="1340">1340</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xl-4 mb-3">
                                                                <label class="form-control-label">مشغول فعالیت در
                                                                    این
                                                                    مجموعه هستم</label>
                                                                <input type="checkbox" class="form-control" id="working_now" />
                                                            </div>
                                                        </div>
                                                        <div class="row p-1">
                                                            <h5 class="w-100">وظایف / دستاوردها</h5>
                                                            <ul class="w-100">
                                                                <li>در این بخش می توانید خیلی خلاصه و کوتاه، وظایف و
                                                                    دستاوردهای خود را به تفکیک و جداگانه لیست کنید.
                                                                </li>
                                                                <li>برای تفکیک از دکمه Enter استفاده کنید</li>
                                                            </ul>
                                                            <textarea id="tasks" class="form-control col-xl-12 mt-3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>`
        } else {
            return `<div class="row bg-white mb-3 p-3" style="position:relative">
                                                    <i class="la la-times"></i>
                                                    <div class="col-xl-12">
                                                        <div class="row">
                                                            <div class="col-xl-3 mb-3">
                                                                <label class="form-control-label">سِمت شغلی
                                                                    مربوطه</label>
                                                                <input type="text" class="form-control" id="job_position" value="${data.job_position == null ? '' : data.job_position}" />
                                                            </div>
                                                            <div class="col-xl-3 mb-3">
                                                                <label class="form-control-label">عنوان محل
                                                                    کار</label>
                                                                <input type="text" class="form-control" id="workplace_title" value="${data.workplace_title == null ? '' : data.workplace_title}" />
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">کشور</label>
                                                                <input type="text" class="form-control" id="country" value="${data.country == null ? '' : data.country}" />
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">استان</label>
                                                                <input type="text" class="form-control" id="state" value="${data.state == null ? '' : data.state}" />
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">شهر</label>
                                                                <input type="text" class="form-control" id="city" value="${data.city == null ? '' : data.city}" />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">شروع</label>
                                                                <select id="start_month" class="custom-select form-control">
                                                                    <option value="" selected="">
                                                                    </option>
                                                                    <option value="1"${data.start_month == "1" ? " selected" : ""}>فروردین</option>
                                                                    <option value="2"${data.start_month == "2" ? " selected" : ""}>اردیبهشت</option>
                                                                    <option value="3"${data.start_month == "3" ? " selected" : ""}>خرداد</option>
                                                                    <option value="4"${data.start_month == "4" ? " selected" : ""}>تیر</option>
                                                                    <option value="5"${data.start_month == "5" ? " selected" : ""}>مرداد</option>
                                                                    <option value="6"${data.start_month == "6" ? " selected" : ""}>شهریور</option>
                                                                    <option value="7"${data.start_month == "7" ? " selected" : ""}>مهر</option>
                                                                    <option value="8"${data.start_month == "8" ? " selected" : ""}>آبان</option>
                                                                    <option value="9"${data.start_month == "9" ? " selected" : ""}>آذر</option>
                                                                    <option value="10"${data.start_month == "10" ? " selected" : ""}>دی</option>
                                                                    <option value="11"${data.start_month == "11" ? " selected" : ""}>بهمن</option>
                                                                    <option value="12"${data.start_month == "12" ? " selected" : ""}>اسفند</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">&nbsp;</label>
                                                                <select id="start_year" class="custom-select form-control">
                                                                    <option value="" selected="">
                                                                    </option>
                                                                    <option value="1401"${data.start_year == "1401" ? " selected" : ""}>1401</option>
                                                                    <option value="1400"${data.start_year == "1400" ? " selected" : ""}>1400</option>
                                                                    <option value="1399"${data.start_year == "1399" ? " selected" : ""}>1399</option>
                                                                    <option value="1398"${data.start_year == "1398" ? " selected" : ""}>1398</option>
                                                                    <option value="1397"${data.start_year == "1397" ? " selected" : ""}>1397</option>
                                                                    <option value="1396"${data.start_year == "1396" ? " selected" : ""}>1396</option>
                                                                    <option value="1395"${data.start_year == "1395" ? " selected" : ""}>1395</option>
                                                                    <option value="1394"${data.start_year == "1394" ? " selected" : ""}>1394</option>
                                                                    <option value="1393"${data.start_year == "1393" ? " selected" : ""}>1393</option>
                                                                    <option value="1392"${data.start_year == "1392" ? " selected" : ""}>1392</option>
                                                                    <option value="1391"${data.start_year == "1391" ? " selected" : ""}>1391</option>
                                                                    <option value="1390"${data.start_year == "1390" ? " selected" : ""}>1390</option>
                                                                    <option value="1389"${data.start_year == "1389" ? " selected" : ""}>1389</option>
                                                                    <option value="1388"${data.start_year == "1388" ? " selected" : ""}>1388</option>
                                                                    <option value="1387"${data.start_year == "1387" ? " selected" : ""}>1387</option>
                                                                    <option value="1386"${data.start_year == "1386" ? " selected" : ""}>1386</option>
                                                                    <option value="1385"${data.start_year == "1385" ? " selected" : ""}>1385</option>
                                                                    <option value="1384"${data.start_year == "1384" ? " selected" : ""}>1384</option>
                                                                    <option value="1383"${data.start_year == "1383" ? " selected" : ""}>1383</option>
                                                                    <option value="1382"${data.start_year == "1382" ? " selected" : ""}>1382</option>
                                                                    <option value="1381"${data.start_year == "1381" ? " selected" : ""}>1381</option>
                                                                    <option value="1380"${data.start_year == "1380" ? " selected" : ""}>1380</option>
                                                                    <option value="1379"${data.start_year == "1379" ? " selected" : ""}>1379</option>
                                                                    <option value="1378"${data.start_year == "1378" ? " selected" : ""}>1378</option>
                                                                    <option value="1377"${data.start_year == "1377" ? " selected" : ""}>1377</option>
                                                                    <option value="1376"${data.start_year == "1376" ? " selected" : ""}>1376</option>
                                                                    <option value="1375"${data.start_year == "1375" ? " selected" : ""}>1375</option>
                                                                    <option value="1374"${data.start_year == "1374" ? " selected" : ""}>1374</option>
                                                                    <option value="1373"${data.start_year == "1373" ? " selected" : ""}>1373</option>
                                                                    <option value="1372"${data.start_year == "1372" ? " selected" : ""}>1372</option>
                                                                    <option value="1371"${data.start_year == "1371" ? " selected" : ""}>1371</option>
                                                                    <option value="1370"${data.start_year == "1370" ? " selected" : ""}>1370</option>
                                                                    <option value="1369"${data.start_year == "1369" ? " selected" : ""}>1369</option>
                                                                    <option value="1368"${data.start_year == "1368" ? " selected" : ""}>1368</option>
                                                                    <option value="1367"${data.start_year == "1367" ? " selected" : ""}>1367</option>
                                                                    <option value="1366"${data.start_year == "1366" ? " selected" : ""}>1366</option>
                                                                    <option value="1365"${data.start_year == "1365" ? " selected" : ""}>1365</option>
                                                                    <option value="1364"${data.start_year == "1364" ? " selected" : ""}>1364</option>
                                                                    <option value="1363"${data.start_year == "1363" ? " selected" : ""}>1363</option>
                                                                    <option value="1362"${data.start_year == "1362" ? " selected" : ""}>1362</option>
                                                                    <option value="1361"${data.start_year == "1361" ? " selected" : ""}>1361</option>
                                                                    <option value="1360"${data.start_year == "1360" ? " selected" : ""}>1360</option>
                                                                    <option value="1359"${data.start_year == "1359" ? " selected" : ""}>1359</option>
                                                                    <option value="1358"${data.start_year == "1358" ? " selected" : ""}>1358</option>
                                                                    <option value="1357"${data.start_year == "1357" ? " selected" : ""}>1357</option>
                                                                    <option value="1356"${data.start_year == "1356" ? " selected" : ""}>1356</option>
                                                                    <option value="1355"${data.start_year == "1355" ? " selected" : ""}>1355</option>
                                                                    <option value="1354"${data.start_year == "1354" ? " selected" : ""}>1354</option>
                                                                    <option value="1353"${data.start_year == "1353" ? " selected" : ""}>1353</option>
                                                                    <option value="1352"${data.start_year == "1352" ? " selected" : ""}>1352</option>
                                                                    <option value="1351"${data.start_year == "1351" ? " selected" : ""}>1351</option>
                                                                    <option value="1350"${data.start_year == "1350" ? " selected" : ""}>1350</option>
                                                                    <option value="1349"${data.start_year == "1349" ? " selected" : ""}>1349</option>
                                                                    <option value="1348"${data.start_year == "1348" ? " selected" : ""}>1348</option>
                                                                    <option value="1347"${data.start_year == "1347" ? " selected" : ""}>1347</option>
                                                                    <option value="1346"${data.start_year == "1346" ? " selected" : ""}>1346</option>
                                                                    <option value="1345"${data.start_year == "1345" ? " selected" : ""}>1345</option>
                                                                    <option value="1344"${data.start_year == "1344" ? " selected" : ""}>1344</option>
                                                                    <option value="1343"${data.start_year == "1343" ? " selected" : ""}>1343</option>
                                                                    <option value="1342"${data.start_year == "1342" ? " selected" : ""}>1342</option>
                                                                    <option value="1341"${data.start_year == "1341" ? " selected" : ""}>1341</option>
                                                                    <option value="1340"${data.start_year == "1340" ? " selected" : ""}>1340</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">اتمام</label>
                                                                <select id="end_month" class="custom-select form-control">
                                                                    <option value="" selected="">
                                                                    </option>
                                                                    <option value="1"${data.end_month == "1" ? " selected" : ""}>فروردین</option>
                                                                    <option value="2"${data.end_month == "2" ? " selected" : ""}>اردیبهشت</option>
                                                                    <option value="3"${data.end_month == "3" ? " selected" : ""}>خرداد</option>
                                                                    <option value="4"${data.end_month == "4" ? " selected" : ""}>تیر</option>
                                                                    <option value="5"${data.end_month == "5" ? " selected" : ""}>مرداد</option>
                                                                    <option value="6"${data.end_month == "6" ? " selected" : ""}>شهریور</option>
                                                                    <option value="7"${data.end_month == "7" ? " selected" : ""}>مهر</option>
                                                                    <option value="8"${data.end_month == "8" ? " selected" : ""}>آبان</option>
                                                                    <option value="9"${data.end_month == "9" ? " selected" : ""}>آذر</option>
                                                                    <option value="10"${data.end_month == "10" ? " selected" : ""}>دی</option>
                                                                    <option value="11"${data.end_month == "11" ? " selected" : ""}>بهمن</option>
                                                                    <option value="12"${data.end_month == "12" ? " selected" : ""}>اسفند</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xl-2 mb-3">
                                                                <label class="form-control-label">&nbsp;</label>
                                                                <select id="end_year" class="custom-select form-control">
                                                                    <option value="" selected="">
                                                                    </option>
                                                                    <option value="1401"${data.end_year == "1401" ? " selected" : ""}>1401</option>
                                                                    <option value="1400"${data.end_year == "1400" ? " selected" : ""}>1400</option>
                                                                    <option value="1399"${data.end_year == "1399" ? " selected" : ""}>1399</option>
                                                                    <option value="1398"${data.end_year == "1398" ? " selected" : ""}>1398</option>
                                                                    <option value="1397"${data.end_year == "1397" ? " selected" : ""}>1397</option>
                                                                    <option value="1396"${data.end_year == "1396" ? " selected" : ""}>1396</option>
                                                                    <option value="1395"${data.end_year == "1395" ? " selected" : ""}>1395</option>
                                                                    <option value="1394"${data.end_year == "1394" ? " selected" : ""}>1394</option>
                                                                    <option value="1393"${data.end_year == "1393" ? " selected" : ""}>1393</option>
                                                                    <option value="1392"${data.end_year == "1392" ? " selected" : ""}>1392</option>
                                                                    <option value="1391"${data.end_year == "1391" ? " selected" : ""}>1391</option>
                                                                    <option value="1390"${data.end_year == "1390" ? " selected" : ""}>1390</option>
                                                                    <option value="1389"${data.end_year == "1389" ? " selected" : ""}>1389</option>
                                                                    <option value="1388"${data.end_year == "1388" ? " selected" : ""}>1388</option>
                                                                    <option value="1387"${data.end_year == "1387" ? " selected" : ""}>1387</option>
                                                                    <option value="1386"${data.end_year == "1386" ? " selected" : ""}>1386</option>
                                                                    <option value="1385"${data.end_year == "1385" ? " selected" : ""}>1385</option>
                                                                    <option value="1384"${data.end_year == "1384" ? " selected" : ""}>1384</option>
                                                                    <option value="1383"${data.end_year == "1383" ? " selected" : ""}>1383</option>
                                                                    <option value="1382"${data.end_year == "1382" ? " selected" : ""}>1382</option>
                                                                    <option value="1381"${data.end_year == "1381" ? " selected" : ""}>1381</option>
                                                                    <option value="1380"${data.end_year == "1380" ? " selected" : ""}>1380</option>
                                                                    <option value="1379"${data.end_year == "1379" ? " selected" : ""}>1379</option>
                                                                    <option value="1378"${data.end_year == "1378" ? " selected" : ""}>1378</option>
                                                                    <option value="1377"${data.end_year == "1377" ? " selected" : ""}>1377</option>
                                                                    <option value="1376"${data.end_year == "1376" ? " selected" : ""}>1376</option>
                                                                    <option value="1375"${data.end_year == "1375" ? " selected" : ""}>1375</option>
                                                                    <option value="1374"${data.end_year == "1374" ? " selected" : ""}>1374</option>
                                                                    <option value="1373"${data.end_year == "1373" ? " selected" : ""}>1373</option>
                                                                    <option value="1372"${data.end_year == "1372" ? " selected" : ""}>1372</option>
                                                                    <option value="1371"${data.end_year == "1371" ? " selected" : ""}>1371</option>
                                                                    <option value="1370"${data.end_year == "1370" ? " selected" : ""}>1370</option>
                                                                    <option value="1369"${data.end_year == "1369" ? " selected" : ""}>1369</option>
                                                                    <option value="1368"${data.end_year == "1368" ? " selected" : ""}>1368</option>
                                                                    <option value="1367"${data.end_year == "1367" ? " selected" : ""}>1367</option>
                                                                    <option value="1366"${data.end_year == "1366" ? " selected" : ""}>1366</option>
                                                                    <option value="1365"${data.end_year == "1365" ? " selected" : ""}>1365</option>
                                                                    <option value="1364"${data.end_year == "1364" ? " selected" : ""}>1364</option>
                                                                    <option value="1363"${data.end_year == "1363" ? " selected" : ""}>1363</option>
                                                                    <option value="1362"${data.end_year == "1362" ? " selected" : ""}>1362</option>
                                                                    <option value="1361"${data.end_year == "1361" ? " selected" : ""}>1361</option>
                                                                    <option value="1360"${data.end_year == "1360" ? " selected" : ""}>1360</option>
                                                                    <option value="1359"${data.end_year == "1359" ? " selected" : ""}>1359</option>
                                                                    <option value="1358"${data.end_year == "1358" ? " selected" : ""}>1358</option>
                                                                    <option value="1357"${data.end_year == "1357" ? " selected" : ""}>1357</option>
                                                                    <option value="1356"${data.end_year == "1356" ? " selected" : ""}>1356</option>
                                                                    <option value="1355"${data.end_year == "1355" ? " selected" : ""}>1355</option>
                                                                    <option value="1354"${data.end_year == "1354" ? " selected" : ""}>1354</option>
                                                                    <option value="1353"${data.end_year == "1353" ? " selected" : ""}>1353</option>
                                                                    <option value="1352"${data.end_year == "1352" ? " selected" : ""}>1352</option>
                                                                    <option value="1351"${data.end_year == "1351" ? " selected" : ""}>1351</option>
                                                                    <option value="1350"${data.end_year == "1350" ? " selected" : ""}>1350</option>
                                                                    <option value="1349"${data.end_year == "1349" ? " selected" : ""}>1349</option>
                                                                    <option value="1348"${data.end_year == "1348" ? " selected" : ""}>1348</option>
                                                                    <option value="1347"${data.end_year == "1347" ? " selected" : ""}>1347</option>
                                                                    <option value="1346"${data.end_year == "1346" ? " selected" : ""}>1346</option>
                                                                    <option value="1345"${data.end_year == "1345" ? " selected" : ""}>1345</option>
                                                                    <option value="1344"${data.end_year == "1344" ? " selected" : ""}>1344</option>
                                                                    <option value="1343"${data.end_year == "1343" ? " selected" : ""}>1343</option>
                                                                    <option value="1342"${data.end_year == "1342" ? " selected" : ""}>1342</option>
                                                                    <option value="1341"${data.end_year == "1341" ? " selected" : ""}>1341</option>
                                                                    <option value="1340"${data.end_year == "1340" ? " selected" : ""}>1340</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xl-4 mb-3">
                                                                <label class="form-control-label">مشغول فعالیت در
                                                                    این
                                                                    مجموعه هستم</label>
                                                                <input type="checkbox" class="form-control" id="working_now"${data.working_now == "true" ? ' checked' : ''} />
                                                            </div>
                                                        </div>
                                                        <div class="row p-1">
                                                            <h5 class="w-100">وظایف / دستاوردها</h5>
                                                            <ul class="w-100">
                                                                <li>در این بخش می توانید خیلی خلاصه و کوتاه، وظایف و
                                                                    دستاوردهای خود را به تفکیک و جداگانه لیست کنید.
                                                                </li>
                                                                <li>برای تفکیک از دکمه Enter استفاده کنید</li>
                                                            </ul>
                                                            <textarea id="tasks" class="form-control col-xl-12 mt-3">${data.tasks == null ? '' : data.tasks}</textarea>
                                                        </div>
                                                    </div>
                                                </div>`
        }
    }

        function lang_list(data) {
        if (data == 'new') {
return `<div class="row bg-white mb-3 p-3" style="position:relative">
                                                        <i class="la la-times"></i>
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">نام زبان</label>
                                                                    <select id="lang_name" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                        </option>
                                                                        <option value="English">انگلیسی</option>
                                                                        <option value="Arabic">عربی</option>
                                                                        <option value="German">آلمانی</option>
                                                                        <option value="France">فرانسوی</option>
                                                                        <option value="Spanish">اسپانیایی</option>
                                                                        <option value="Russian">روسی</option>
                                                                        <option value="Italy">ایتالیایی</option>
                                                                        <option value="Turkish">ترکی استانبولی</option>
                                                                        <option value="Persian">فارسی</option>
                                                                        <option value="China">چینی</option>
                                                                        <option value="Hebrew">عبری</option>
                                                                        <option value="Azerbaijani">ترکی آذربایجانی
                                                                        </option>
                                                                        <option value="Armenian">ارمنی</option>
                                                                        <option value="Japanese">ژاپنی</option>
                                                                        <option value="Georgian">گرجی</option>
                                                                        <option value="Kurdish">کُردی</option>
                                                                        <option value="Portuguese">پرتغالی</option>
                                                                        <option value="Bengali">بنگالی</option>
                                                                        <option value="Lahnda">لندا</option>
                                                                        <option value="Javanese">جاوه&zwnj;ای</option>
                                                                        <option value="Korean">کره ای</option>
                                                                        <option value="Vietnamese">ویتنامی</option>
                                                                        <option value="Urdu">اردو</option>
                                                                        <option value="Hindi">هندی</option>
                                                                        <option value="Egyptian">مصری</option>
                                                                        <option value="Telugu">تلوگو</option>
                                                                        <option value="Gujarati">گجراتی</option>
                                                                        <option value="Tamil">تامیلی</option>
                                                                        <option value="Marathi">مراتی</option>
                                                                        <option value="Hungarian">مجاری</option>
                                                                        <option value="Swedish">سوئدی</option>
                                                                        <option value="Pashto">پشتو</option>
                                                                        <option value="Greek">یونانی</option>
                                                                        <option value="Dutch">هلندی</option>
                                                                        <option value="Danish">دانمارکی</option>
                                                                        <option value="Latin">لاتین</option>
                                                                        <option value="Serbian">صربی</option>
                                                                        <option value="Croatian">کرواتی</option>
                                                                        <option value="Catalan">کاتالان</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">مهارت
                                                                        خواندن</label>
                                                                    <select id="reading_skills" class="custom-select form-control">
                                                                        <option selected="" value="1">مبتدی
                                                                        </option>
                                                                        <option value="2">آشنایی نسبی</option>
                                                                        <option value="3">متوسط</option>
                                                                        <option value="4">پیشرفته</option>
                                                                        <option value="5">مسلط</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">مهارت
                                                                        نوشتن</label>
                                                                    <select id="writing_skills" class="custom-select form-control">
                                                                        <option selected="" value="1">مبتدی
                                                                        </option>
                                                                        <option value="2">آشنایی نسبی</option>
                                                                        <option value="3">متوسط</option>
                                                                        <option value="4">پیشرفته</option>
                                                                        <option value="5">مسلط</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">مهارت
                                                                        شنیداری</label>
                                                                    <select id="listening_skills" class="custom-select form-control">
                                                                        <option selected="" value="1">مبتدی
                                                                        </option>
                                                                        <option value="2">آشنایی نسبی</option>
                                                                        <option value="3">متوسط</option>
                                                                        <option value="4">پیشرفته</option>
                                                                        <option value="5">مسلط</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">مهارت
                                                                        گفتاری</label>
                                                                    <select id="speaking_skills" class="custom-select form-control">
                                                                        <option selected="" value="1">مبتدی
                                                                        </option>
                                                                        <option value="2">آشنایی نسبی</option>
                                                                        <option value="3">متوسط</option>
                                                                        <option value="4">پیشرفته</option>
                                                                        <option value="5">مسلط</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">توضیحات</label>
                                                                    <input type="text" class="form-control" placeholder="مثلا: نمره تافل 95" id="description" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>`
        } else {
            return `<div class="row bg-white mb-3 p-3" style="position:relative">
                                                        <i class="la la-times"></i>
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">نام زبان</label>
                                                                    <select id="lang_name" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                        </option>
                                                                        <option value="English"${data.lang_name == "English" ? " selected" : ""}>انگلیسی</option>
                                                                        <option value="Arabic"${data.lang_name == "Arabic" ? " selected" : ""}>عربی</option>
                                                                        <option value="German"${data.lang_name == "German" ? " selected" : ""}>آلمانی</option>
                                                                        <option value="France"${data.lang_name == "France" ? " selected" : ""}>فرانسوی</option>
                                                                        <option value="Spanish"${data.lang_name == "Spanish" ? " selected" : ""}>اسپانیایی</option>
                                                                        <option value="Russian"${data.lang_name == "Russian" ? " selected" : ""}>روسی</option>
                                                                        <option value="Italy"${data.lang_name == "Italy" ? " selected" : ""}>ایتالیایی</option>
                                                                        <option value="Turkish"${data.lang_name == "Turkish" ? " selected" : ""}>ترکی استانبولی</option>
                                                                        <option value="Persian"${data.lang_name == "Persian" ? " selected" : ""}>فارسی</option>
                                                                        <option value="China"${data.lang_name == "China" ? " selected" : ""}>چینی</option>
                                                                        <option value="Hebrew"${data.lang_name == "Hebrew" ? " selected" : ""}>عبری</option>
                                                                        <option value="Azerbaijani"${data.lang_name == "Azerbaijani" ? " selected" : ""}>ترکی آذربایجانی
                                                                        </option>
                                                                        <option value="Armenian"${data.lang_name == "Armenian" ? " selected" : ""}>ارمنی</option>
                                                                        <option value="Japanese"${data.lang_name == "Japanese" ? " selected" : ""}>ژاپنی</option>
                                                                        <option value="Georgian"${data.lang_name == "Georgian" ? " selected" : ""}>گرجی</option>
                                                                        <option value="Kurdish"${data.lang_name == "Kurdish" ? " selected" : ""}>کُردی</option>
                                                                        <option value="Portuguese"${data.lang_name == "Portuguese" ? " selected" : ""}>پرتغالی</option>
                                                                        <option value="Bengali"${data.lang_name == "Bengali" ? " selected" : ""}>بنگالی</option>
                                                                        <option value="Lahnda"${data.lang_name == "Lahnda" ? " selected" : ""}>لندا</option>
                                                                        <option value="Javanese"${data.lang_name == "Javanese" ? " selected" : ""}>جاوه&zwnj;ای</option>
                                                                        <option value="Korean"${data.lang_name == "Korean" ? " selected" : ""}>کره ای</option>
                                                                        <option value="Vietnamese"${data.lang_name == "Vietnamese" ? " selected" : ""}>ویتنامی</option>
                                                                        <option value="Urdu"${data.lang_name == "Urdu" ? " selected" : ""}>اردو</option>
                                                                        <option value="Hindi"${data.lang_name == "Hindi" ? " selected" : ""}>هندی</option>
                                                                        <option value="Egyptian"${data.lang_name == "Egyptian" ? " selected" : ""}>مصری</option>
                                                                        <option value="Telugu"${data.lang_name == "Telugu" ? " selected" : ""}>تلوگو</option>
                                                                        <option value="Gujarati"${data.lang_name == "Gujarati" ? " selected" : ""}>گجراتی</option>
                                                                        <option value="Tamil"${data.lang_name == "Tamil" ? " selected" : ""}>تامیلی</option>
                                                                        <option value="Marathi"${data.lang_name == "Marathi" ? " selected" : ""}>مراتی</option>
                                                                        <option value="Hungarian"${data.lang_name == "Hungarian" ? " selected" : ""}>مجاری</option>
                                                                        <option value="Swedish"${data.lang_name == "Swedish" ? " selected" : ""}>سوئدی</option>
                                                                        <option value="Greek"${data.lang_name == "Greek" ? " selected" : ""}>یونانی</option>
                                                                        <option value="Dutch"${data.lang_name == "Dutch" ? " selected" : ""}>هلندی</option>
                                                                        <option value="Danish"${data.lang_name == "Danish" ? " selected" : ""}>دانمارکی</option>
                                                                        <option value="Latin"${data.lang_name == "Latin" ? " selected" : ""}>لاتین</option>
                                                                        <option value="Serbian"${data.lang_name == "Serbian" ? " selected" : ""}>صربی</option>
                                                                        <option value="Croatian"${data.lang_name == "Croatian" ? " selected" : ""}>کرواتی</option>
                                                                        <option value="Catalan"${data.lang_name == "Catalan" ? " selected" : ""}>کاتالان</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">مهارت
                                                                        خواندن</label>
                                                                    <select id="reading_skills" class="custom-select form-control">
                                                                        <option selected="" value="1"${data.reading_skills == "1" ? " selected" : ""}>مبتدی</option>
                                                                        <option value="2"${data.reading_skills == "2" ? " selected" : ""}>آشنایی نسبی</option>
                                                                        <option value="3"${data.reading_skills == "3" ? " selected" : ""}>متوسط</option>
                                                                        <option value="4"${data.reading_skills == "4" ? " selected" : ""}>پیشرفته</option>
                                                                        <option value="5"${data.reading_skills == "5" ? " selected" : ""}>مسلط</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">مهارت
                                                                        نوشتن</label>
                                                                    <select id="writing_skills" class="custom-select form-control">
                                                                        <option selected="" value="1"${data.writing_skills == "1" ? " selected" : ""}>مبتدی</option>
                                                                        <option value="2"${data.writing_skills == "2" ? " selected" : ""}>آشنایی نسبی</option>
                                                                        <option value="3"${data.writing_skills == "3" ? " selected" : ""}>متوسط</option>
                                                                        <option value="4"${data.writing_skills == "4" ? " selected" : ""}>پیشرفته</option>
                                                                        <option value="5"${data.writing_skills == "5" ? " selected" : ""}>مسلط</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">مهارت
                                                                        شنیداری</label>
                                                                    <select id="listening_skills" class="custom-select form-control">
                                                                        <option selected="" value="1"${data.listening_skills == "1" ? " selected" : ""}>مبتدی</option>
                                                                        <option value="2"${data.listening_skills == "2" ? " selected" : ""}>آشنایی نسبی</option>
                                                                        <option value="3"${data.listening_skills == "3" ? " selected" : ""}>متوسط</option>
                                                                        <option value="4"${data.listening_skills == "4" ? " selected" : ""}>پیشرفته</option>
                                                                        <option value="5"${data.listening_skills == "5" ? " selected" : ""}>مسلط</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">مهارت
                                                                        گفتاری</label>
                                                                    <select id="speaking_skills" class="custom-select form-control">
                                                                        <option selected="" value="1"${data.speaking_skills == "1" ? " selected" : ""}>مبتدی</option>
                                                                        <option value="2"${data.speaking_skills == "2" ? " selected" : ""}>آشنایی نسبی</option>
                                                                        <option value="3"${data.speaking_skills == "3" ? " selected" : ""}>متوسط</option>
                                                                        <option value="4"${data.speaking_skills == "4" ? " selected" : ""}>پیشرفته</option>
                                                                        <option value="5"${data.speaking_skills == "5" ? " selected" : ""}>مسلط</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">توضیحات</label>
                                                                    <input type="text" class="form-control" placeholder="مثلا: نمره تافل 95" id="description" value="${data.description == null ? '' : data.description}" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>`

        }
    }

function skills_list(data) {
    if (data == 'new') {
return `<div class="col-xl-6 bg-white p-3" style="position:relative">
                                                        <i class="la la-times"></i>
                                                        <div class="row p-3">
                                                            <div class="col-xl-8">
                                                                <label class="form-control-label">نام
                                                                    مهارت</label>
                                                                <input type="text" class="form-control" id="skill" />
                                                            </div>
                                                            <div class="col-xl-4">
                                                                <label class="form-control-label">سطح</label>
                                                                <select id="skill_level" class="custom-select form-control">
                                                                    <option selected="" value="1">مبتدی
                                                                    </option>
                                                                    <option value="2">آشنایی نسبی</option>
                                                                    <option value="3">متوسط</option>
                                                                    <option value="4">پیشرفته</option>
                                                                    <option value="5">مسلط</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>`
    } else {
        return `<div class="col-xl-6 bg-white p-3" style="position:relative">
                                                        <i class="la la-times"></i>
                                                        <div class="row p-3">
                                                            <div class="col-xl-8">
                                                                <label class="form-control-label">نام
                                                                    مهارت</label>
                                                                <input type="text" class="form-control" id="skill" value="${data.skill == null ? '' : data.skill}" />
                                                            </div>
                                                            <div class="col-xl-4">
                                                                <label class="form-control-label">سطح</label>
                                                                <select id="skill_level" class="custom-select form-control">
                                                                    <option selected="" value="1"${data.skill_level == "1" ? " selected" : ""}>مبتدی</option>
                                                                        <option value="2"${data.skill_level == "2" ? " selected" : ""}>آشنایی نسبی</option>
                                                                        <option value="3"${data.skill_level == "3" ? " selected" : ""}>متوسط</option>
                                                                        <option value="4"${data.skill_level == "4" ? " selected" : ""}>پیشرفته</option>
                                                                        <option value="5"${data.skill_level == "5" ? " selected" : ""}>مسلط</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>`
        }
    }

function projects_list(data) {
    if (data == 'new') {
return `<div class="row bg-white mb-3 p-3" style="position:relative">
                                                        <i class="la la-times"></i>
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-xl-8 mb-3">
                                                                    <label class="form-control-label">عنوان</label>
                                                                    <input type="text" class="form-control" id="project_title" />
                                                                </div>
                                                                <div class="col-xl-4 mb-3">
                                                                    <label class="form-control-label">کارفرما / درخواست
                                                                        کننده</label>
                                                                    <input type="text" class="form-control" id="employer" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-8 mb-3">
                                                                    <label class="form-control-label">لینک مرتبط</label>
                                                                    <input type="text" class="form-control" dir="ltr" id="link" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">تاریخ</label>
                                                                    <select id="date_month" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                        </option>
                                                                        <option value="1">فروردین</option>
                                                                        <option value="2">اردیبهشت</option>
                                                                        <option value="3">خرداد</option>
                                                                        <option value="4">تیر</option>
                                                                        <option value="5">مرداد</option>
                                                                        <option value="6">شهریور</option>
                                                                        <option value="7">مهر</option>
                                                                        <option value="8">آبان</option>
                                                                        <option value="9">آذر</option>
                                                                        <option value="10">دی</option>
                                                                        <option value="11">بهمن</option>
                                                                        <option value="12">اسفند</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">&nbsp;</label>
                                                                    <select id="date_year" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                        </option>
                                                                        <option value="1401">1401</option>
                                                                        <option value="1400">1400</option>
                                                                        <option value="1399">1399</option>
                                                                        <option value="1398">1398</option>
                                                                        <option value="1397">1397</option>
                                                                        <option value="1396">1396</option>
                                                                        <option value="1395">1395</option>
                                                                        <option value="1394">1394</option>
                                                                        <option value="1393">1393</option>
                                                                        <option value="1392">1392</option>
                                                                        <option value="1391">1391</option>
                                                                        <option value="1390">1390</option>
                                                                        <option value="1389">1389</option>
                                                                        <option value="1388">1388</option>
                                                                        <option value="1387">1387</option>
                                                                        <option value="1386">1386</option>
                                                                        <option value="1385">1385</option>
                                                                        <option value="1384">1384</option>
                                                                        <option value="1383">1383</option>
                                                                        <option value="1382">1382</option>
                                                                        <option value="1381">1381</option>
                                                                        <option value="1380">1380</option>
                                                                        <option value="1379">1379</option>
                                                                        <option value="1378">1378</option>
                                                                        <option value="1377">1377</option>
                                                                        <option value="1376">1376</option>
                                                                        <option value="1375">1375</option>
                                                                        <option value="1374">1374</option>
                                                                        <option value="1373">1373</option>
                                                                        <option value="1372">1372</option>
                                                                        <option value="1371">1371</option>
                                                                        <option value="1370">1370</option>
                                                                        <option value="1369">1369</option>
                                                                        <option value="1368">1368</option>
                                                                        <option value="1367">1367</option>
                                                                        <option value="1366">1366</option>
                                                                        <option value="1365">1365</option>
                                                                        <option value="1364">1364</option>
                                                                        <option value="1363">1363</option>
                                                                        <option value="1362">1362</option>
                                                                        <option value="1361">1361</option>
                                                                        <option value="1360">1360</option>
                                                                        <option value="1359">1359</option>
                                                                        <option value="1358">1358</option>
                                                                        <option value="1357">1357</option>
                                                                        <option value="1356">1356</option>
                                                                        <option value="1355">1355</option>
                                                                        <option value="1354">1354</option>
                                                                        <option value="1353">1353</option>
                                                                        <option value="1352">1352</option>
                                                                        <option value="1351">1351</option>
                                                                        <option value="1350">1350</option>
                                                                        <option value="1349">1349</option>
                                                                        <option value="1348">1348</option>
                                                                        <option value="1347">1347</option>
                                                                        <option value="1346">1346</option>
                                                                        <option value="1345">1345</option>
                                                                        <option value="1344">1344</option>
                                                                        <option value="1343">1343</option>
                                                                        <option value="1342">1342</option>
                                                                        <option value="1341">1341</option>
                                                                        <option value="1340">1340</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label">توضیحات</label>
                                                                    <textarea id="description" class="form-control col-xl-12"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>`
    } else {
        return `<div class="row bg-white mb-3 p-3" style="position:relative">
                                                        <i class="la la-times"></i>
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-xl-8 mb-3">
                                                                    <label class="form-control-label">عنوان</label>
                                                                    <input type="text" class="form-control" id="project_title" value="${data.project_title == null ? '' : data.project_title}" />
                                                                </div>
                                                                <div class="col-xl-4 mb-3">
                                                                    <label class="form-control-label">کارفرما / درخواست
                                                                        کننده</label>
                                                                    <input type="text" class="form-control" id="employer" value="${data.employer == null ? '' : data.employer}" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-8 mb-3">
                                                                    <label class="form-control-label">لینک مرتبط</label>
                                                                    <input type="text" class="form-control" dir="ltr" id="link" value="${data.link == null ? '' : data.link}" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">تاریخ</label>
                                                                    <select id="date_month" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                    </option>
                                                                    <option value="1"${data.date_month == "1" ? " selected" : ""}>فروردین</option>
                                                                    <option value="2"${data.date_month == "2" ? " selected" : ""}>اردیبهشت</option>
                                                                    <option value="3"${data.date_month == "3" ? " selected" : ""}>خرداد</option>
                                                                    <option value="4"${data.date_month == "4" ? " selected" : ""}>تیر</option>
                                                                    <option value="5"${data.date_month == "5" ? " selected" : ""}>مرداد</option>
                                                                    <option value="6"${data.date_month == "6" ? " selected" : ""}>شهریور</option>
                                                                    <option value="7"${data.date_month == "7" ? " selected" : ""}>مهر</option>
                                                                    <option value="8"${data.date_month == "8" ? " selected" : ""}>آبان</option>
                                                                    <option value="9"${data.date_month == "9" ? " selected" : ""}>آذر</option>
                                                                    <option value="10"${data.date_month == "10" ? " selected" : ""}>دی</option>
                                                                    <option value="11"${data.date_month == "11" ? " selected" : ""}>بهمن</option>
                                                                    <option value="12"${data.date_month == "12" ? " selected" : ""}>اسفند</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">&nbsp;</label>
                                                                    <select id="date_year" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                    </option>
                                                                    <option value="1401"${data.date_year == "1401" ? " selected" : ""}>1401</option>
                                                                    <option value="1400"${data.date_year == "1400" ? " selected" : ""}>1400</option>
                                                                    <option value="1399"${data.date_year == "1399" ? " selected" : ""}>1399</option>
                                                                    <option value="1398"${data.date_year == "1398" ? " selected" : ""}>1398</option>
                                                                    <option value="1397"${data.date_year == "1397" ? " selected" : ""}>1397</option>
                                                                    <option value="1396"${data.date_year == "1396" ? " selected" : ""}>1396</option>
                                                                    <option value="1395"${data.date_year == "1395" ? " selected" : ""}>1395</option>
                                                                    <option value="1394"${data.date_year == "1394" ? " selected" : ""}>1394</option>
                                                                    <option value="1393"${data.date_year == "1393" ? " selected" : ""}>1393</option>
                                                                    <option value="1392"${data.date_year == "1392" ? " selected" : ""}>1392</option>
                                                                    <option value="1391"${data.date_year == "1391" ? " selected" : ""}>1391</option>
                                                                    <option value="1390"${data.date_year == "1390" ? " selected" : ""}>1390</option>
                                                                    <option value="1389"${data.date_year == "1389" ? " selected" : ""}>1389</option>
                                                                    <option value="1388"${data.date_year == "1388" ? " selected" : ""}>1388</option>
                                                                    <option value="1387"${data.date_year == "1387" ? " selected" : ""}>1387</option>
                                                                    <option value="1386"${data.date_year == "1386" ? " selected" : ""}>1386</option>
                                                                    <option value="1385"${data.date_year == "1385" ? " selected" : ""}>1385</option>
                                                                    <option value="1384"${data.date_year == "1384" ? " selected" : ""}>1384</option>
                                                                    <option value="1383"${data.date_year == "1383" ? " selected" : ""}>1383</option>
                                                                    <option value="1382"${data.date_year == "1382" ? " selected" : ""}>1382</option>
                                                                    <option value="1381"${data.date_year == "1381" ? " selected" : ""}>1381</option>
                                                                    <option value="1380"${data.date_year == "1380" ? " selected" : ""}>1380</option>
                                                                    <option value="1379"${data.date_year == "1379" ? " selected" : ""}>1379</option>
                                                                    <option value="1378"${data.date_year == "1378" ? " selected" : ""}>1378</option>
                                                                    <option value="1377"${data.date_year == "1377" ? " selected" : ""}>1377</option>
                                                                    <option value="1376"${data.date_year == "1376" ? " selected" : ""}>1376</option>
                                                                    <option value="1375"${data.date_year == "1375" ? " selected" : ""}>1375</option>
                                                                    <option value="1374"${data.date_year == "1374" ? " selected" : ""}>1374</option>
                                                                    <option value="1373"${data.date_year == "1373" ? " selected" : ""}>1373</option>
                                                                    <option value="1372"${data.date_year == "1372" ? " selected" : ""}>1372</option>
                                                                    <option value="1371"${data.date_year == "1371" ? " selected" : ""}>1371</option>
                                                                    <option value="1370"${data.date_year == "1370" ? " selected" : ""}>1370</option>
                                                                    <option value="1369"${data.date_year == "1369" ? " selected" : ""}>1369</option>
                                                                    <option value="1368"${data.date_year == "1368" ? " selected" : ""}>1368</option>
                                                                    <option value="1367"${data.date_year == "1367" ? " selected" : ""}>1367</option>
                                                                    <option value="1366"${data.date_year == "1366" ? " selected" : ""}>1366</option>
                                                                    <option value="1365"${data.date_year == "1365" ? " selected" : ""}>1365</option>
                                                                    <option value="1364"${data.date_year == "1364" ? " selected" : ""}>1364</option>
                                                                    <option value="1363"${data.date_year == "1363" ? " selected" : ""}>1363</option>
                                                                    <option value="1362"${data.date_year == "1362" ? " selected" : ""}>1362</option>
                                                                    <option value="1361"${data.date_year == "1361" ? " selected" : ""}>1361</option>
                                                                    <option value="1360"${data.date_year == "1360" ? " selected" : ""}>1360</option>
                                                                    <option value="1359"${data.date_year == "1359" ? " selected" : ""}>1359</option>
                                                                    <option value="1358"${data.date_year == "1358" ? " selected" : ""}>1358</option>
                                                                    <option value="1357"${data.date_year == "1357" ? " selected" : ""}>1357</option>
                                                                    <option value="1356"${data.date_year == "1356" ? " selected" : ""}>1356</option>
                                                                    <option value="1355"${data.date_year == "1355" ? " selected" : ""}>1355</option>
                                                                    <option value="1354"${data.date_year == "1354" ? " selected" : ""}>1354</option>
                                                                    <option value="1353"${data.date_year == "1353" ? " selected" : ""}>1353</option>
                                                                    <option value="1352"${data.date_year == "1352" ? " selected" : ""}>1352</option>
                                                                    <option value="1351"${data.date_year == "1351" ? " selected" : ""}>1351</option>
                                                                    <option value="1350"${data.date_year == "1350" ? " selected" : ""}>1350</option>
                                                                    <option value="1349"${data.date_year == "1349" ? " selected" : ""}>1349</option>
                                                                    <option value="1348"${data.date_year == "1348" ? " selected" : ""}>1348</option>
                                                                    <option value="1347"${data.date_year == "1347" ? " selected" : ""}>1347</option>
                                                                    <option value="1346"${data.date_year == "1346" ? " selected" : ""}>1346</option>
                                                                    <option value="1345"${data.date_year == "1345" ? " selected" : ""}>1345</option>
                                                                    <option value="1344"${data.date_year == "1344" ? " selected" : ""}>1344</option>
                                                                    <option value="1343"${data.date_year == "1343" ? " selected" : ""}>1343</option>
                                                                    <option value="1342"${data.date_year == "1342" ? " selected" : ""}>1342</option>
                                                                    <option value="1341"${data.date_year == "1341" ? " selected" : ""}>1341</option>
                                                                    <option value="1340"${data.date_year == "1340" ? " selected" : ""}>1340</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label">توضیحات</label>
                                                                    <textarea id="description" class="form-control col-xl-12">${data.description == null ? '' : data.description}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>`
        }
    }

function researches_list(data) {
    if (data == 'new') {
        return `<div class="row bg-white mb-3 p-3" style="position:relative">
                                                        <i class="la la-times"></i>
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label">عنوان</label>
                                                                    <input type="text" class="form-control" id="research_title" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">ناشر</label>
                                                                    <input type="text" class="form-control" id="publisher" />
                                                                </div>
                                                                <div class="col-xl-6 mb-3">
                                                                    <label class="form-control-label">لینک مرتبط</label>
                                                                    <input type="text" class="form-control" dir="ltr" id="link" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">تاریخ</label>
                                                                    <select id="date_month" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                        </option>
                                                                        <option value="1">فروردین</option>
                                                                        <option value="2">اردیبهشت</option>
                                                                        <option value="3">خرداد</option>
                                                                        <option value="4">تیر</option>
                                                                        <option value="5">مرداد</option>
                                                                        <option value="6">شهریور</option>
                                                                        <option value="7">مهر</option>
                                                                        <option value="8">آبان</option>
                                                                        <option value="9">آذر</option>
                                                                        <option value="10">دی</option>
                                                                        <option value="11">بهمن</option>
                                                                        <option value="12">اسفند</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">&nbsp;</label>
                                                                    <select id="date_year" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                        </option>
                                                                        <option value="1401">1401</option>
                                                                        <option value="1400">1400</option>
                                                                        <option value="1399">1399</option>
                                                                        <option value="1398">1398</option>
                                                                        <option value="1397">1397</option>
                                                                        <option value="1396">1396</option>
                                                                        <option value="1395">1395</option>
                                                                        <option value="1394">1394</option>
                                                                        <option value="1393">1393</option>
                                                                        <option value="1392">1392</option>
                                                                        <option value="1391">1391</option>
                                                                        <option value="1390">1390</option>
                                                                        <option value="1389">1389</option>
                                                                        <option value="1388">1388</option>
                                                                        <option value="1387">1387</option>
                                                                        <option value="1386">1386</option>
                                                                        <option value="1385">1385</option>
                                                                        <option value="1384">1384</option>
                                                                        <option value="1383">1383</option>
                                                                        <option value="1382">1382</option>
                                                                        <option value="1381">1381</option>
                                                                        <option value="1380">1380</option>
                                                                        <option value="1379">1379</option>
                                                                        <option value="1378">1378</option>
                                                                        <option value="1377">1377</option>
                                                                        <option value="1376">1376</option>
                                                                        <option value="1375">1375</option>
                                                                        <option value="1374">1374</option>
                                                                        <option value="1373">1373</option>
                                                                        <option value="1372">1372</option>
                                                                        <option value="1371">1371</option>
                                                                        <option value="1370">1370</option>
                                                                        <option value="1369">1369</option>
                                                                        <option value="1368">1368</option>
                                                                        <option value="1367">1367</option>
                                                                        <option value="1366">1366</option>
                                                                        <option value="1365">1365</option>
                                                                        <option value="1364">1364</option>
                                                                        <option value="1363">1363</option>
                                                                        <option value="1362">1362</option>
                                                                        <option value="1361">1361</option>
                                                                        <option value="1360">1360</option>
                                                                        <option value="1359">1359</option>
                                                                        <option value="1358">1358</option>
                                                                        <option value="1357">1357</option>
                                                                        <option value="1356">1356</option>
                                                                        <option value="1355">1355</option>
                                                                        <option value="1354">1354</option>
                                                                        <option value="1353">1353</option>
                                                                        <option value="1352">1352</option>
                                                                        <option value="1351">1351</option>
                                                                        <option value="1350">1350</option>
                                                                        <option value="1349">1349</option>
                                                                        <option value="1348">1348</option>
                                                                        <option value="1347">1347</option>
                                                                        <option value="1346">1346</option>
                                                                        <option value="1345">1345</option>
                                                                        <option value="1344">1344</option>
                                                                        <option value="1343">1343</option>
                                                                        <option value="1342">1342</option>
                                                                        <option value="1341">1341</option>
                                                                        <option value="1340">1340</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label">توضیحات</label>
                                                                    <textarea id="description" class="form-control col-xl-12"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>`
    } else {
        return `<div class="row bg-white mb-3 p-3" style="position:relative">
                                                        <i class="la la-times"></i>
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label">عنوان</label>
                                                                    <input type="text" class="form-control" id="research_title" value="${data.research_title == null ? '' : data.research_title}" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">ناشر</label>
                                                                    <input type="text" class="form-control" id="publisher" value="${data.publisher == null ? '' : data.publisher}" />
                                                                </div>
                                                                <div class="col-xl-6 mb-3">
                                                                    <label class="form-control-label">لینک مرتبط</label>
                                                                    <input type="text" class="form-control" dir="ltr" id="link" value="${data.link == null ? '' : data.link}" />
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">تاریخ</label>
                                                                    <select id="date_month" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                        </option>
                                                                        <option value="1"${data.date_month == "1" ? " selected" : ""}>فروردین</option>
                                                                        <option value="2"${data.date_month == "2" ? " selected" : ""}>اردیبهشت</option>
                                                                        <option value="3"${data.date_month == "3" ? " selected" : ""}>خرداد</option>
                                                                        <option value="4"${data.date_month == "4" ? " selected" : ""}>تیر</option>
                                                                        <option value="5"${data.date_month == "5" ? " selected" : ""}>مرداد</option>
                                                                        <option value="6"${data.date_month == "6" ? " selected" : ""}>شهریور</option>
                                                                        <option value="7"${data.date_month == "7" ? " selected" : ""}>مهر</option>
                                                                        <option value="8"${data.date_month == "8" ? " selected" : ""}>آبان</option>
                                                                        <option value="9"${data.date_month == "9" ? " selected" : ""}>آذر</option>
                                                                        <option value="10"${data.date_month == "10" ? " selected" : ""}>دی</option>
                                                                        <option value="11"${data.date_month == "11" ? " selected" : ""}>بهمن</option>
                                                                        <option value="12"${data.date_month == "12" ? " selected" : ""}>اسفند</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-2 mb-3">
                                                                    <label class="form-control-label">&nbsp;</label>
                                                                    <select id="date_year" class="custom-select form-control">
                                                                        <option value="" selected="">
                                                                        </option>
                                                                        <option value="1401"${data.date_year == "1401" ? " selected" : ""}>1401</option>
                                                                    <option value="1400"${data.date_year == "1400" ? " selected" : ""}>1400</option>
                                                                    <option value="1399"${data.date_year == "1399" ? " selected" : ""}>1399</option>
                                                                    <option value="1398"${data.date_year == "1398" ? " selected" : ""}>1398</option>
                                                                    <option value="1397"${data.date_year == "1397" ? " selected" : ""}>1397</option>
                                                                    <option value="1396"${data.date_year == "1396" ? " selected" : ""}>1396</option>
                                                                    <option value="1395"${data.date_year == "1395" ? " selected" : ""}>1395</option>
                                                                    <option value="1394"${data.date_year == "1394" ? " selected" : ""}>1394</option>
                                                                    <option value="1393"${data.date_year == "1393" ? " selected" : ""}>1393</option>
                                                                    <option value="1392"${data.date_year == "1392" ? " selected" : ""}>1392</option>
                                                                    <option value="1391"${data.date_year == "1391" ? " selected" : ""}>1391</option>
                                                                    <option value="1390"${data.date_year == "1390" ? " selected" : ""}>1390</option>
                                                                    <option value="1389"${data.date_year == "1389" ? " selected" : ""}>1389</option>
                                                                    <option value="1388"${data.date_year == "1388" ? " selected" : ""}>1388</option>
                                                                    <option value="1387"${data.date_year == "1387" ? " selected" : ""}>1387</option>
                                                                    <option value="1386"${data.date_year == "1386" ? " selected" : ""}>1386</option>
                                                                    <option value="1385"${data.date_year == "1385" ? " selected" : ""}>1385</option>
                                                                    <option value="1384"${data.date_year == "1384" ? " selected" : ""}>1384</option>
                                                                    <option value="1383"${data.date_year == "1383" ? " selected" : ""}>1383</option>
                                                                    <option value="1382"${data.date_year == "1382" ? " selected" : ""}>1382</option>
                                                                    <option value="1381"${data.date_year == "1381" ? " selected" : ""}>1381</option>
                                                                    <option value="1380"${data.date_year == "1380" ? " selected" : ""}>1380</option>
                                                                    <option value="1379"${data.date_year == "1379" ? " selected" : ""}>1379</option>
                                                                    <option value="1378"${data.date_year == "1378" ? " selected" : ""}>1378</option>
                                                                    <option value="1377"${data.date_year == "1377" ? " selected" : ""}>1377</option>
                                                                    <option value="1376"${data.date_year == "1376" ? " selected" : ""}>1376</option>
                                                                    <option value="1375"${data.date_year == "1375" ? " selected" : ""}>1375</option>
                                                                    <option value="1374"${data.date_year == "1374" ? " selected" : ""}>1374</option>
                                                                    <option value="1373"${data.date_year == "1373" ? " selected" : ""}>1373</option>
                                                                    <option value="1372"${data.date_year == "1372" ? " selected" : ""}>1372</option>
                                                                    <option value="1371"${data.date_year == "1371" ? " selected" : ""}>1371</option>
                                                                    <option value="1370"${data.date_year == "1370" ? " selected" : ""}>1370</option>
                                                                    <option value="1369"${data.date_year == "1369" ? " selected" : ""}>1369</option>
                                                                    <option value="1368"${data.date_year == "1368" ? " selected" : ""}>1368</option>
                                                                    <option value="1367"${data.date_year == "1367" ? " selected" : ""}>1367</option>
                                                                    <option value="1366"${data.date_year == "1366" ? " selected" : ""}>1366</option>
                                                                    <option value="1365"${data.date_year == "1365" ? " selected" : ""}>1365</option>
                                                                    <option value="1364"${data.date_year == "1364" ? " selected" : ""}>1364</option>
                                                                    <option value="1363"${data.date_year == "1363" ? " selected" : ""}>1363</option>
                                                                    <option value="1362"${data.date_year == "1362" ? " selected" : ""}>1362</option>
                                                                    <option value="1361"${data.date_year == "1361" ? " selected" : ""}>1361</option>
                                                                    <option value="1360"${data.date_year == "1360" ? " selected" : ""}>1360</option>
                                                                    <option value="1359"${data.date_year == "1359" ? " selected" : ""}>1359</option>
                                                                    <option value="1358"${data.date_year == "1358" ? " selected" : ""}>1358</option>
                                                                    <option value="1357"${data.date_year == "1357" ? " selected" : ""}>1357</option>
                                                                    <option value="1356"${data.date_year == "1356" ? " selected" : ""}>1356</option>
                                                                    <option value="1355"${data.date_year == "1355" ? " selected" : ""}>1355</option>
                                                                    <option value="1354"${data.date_year == "1354" ? " selected" : ""}>1354</option>
                                                                    <option value="1353"${data.date_year == "1353" ? " selected" : ""}>1353</option>
                                                                    <option value="1352"${data.date_year == "1352" ? " selected" : ""}>1352</option>
                                                                    <option value="1351"${data.date_year == "1351" ? " selected" : ""}>1351</option>
                                                                    <option value="1350"${data.date_year == "1350" ? " selected" : ""}>1350</option>
                                                                    <option value="1349"${data.date_year == "1349" ? " selected" : ""}>1349</option>
                                                                    <option value="1348"${data.date_year == "1348" ? " selected" : ""}>1348</option>
                                                                    <option value="1347"${data.date_year == "1347" ? " selected" : ""}>1347</option>
                                                                    <option value="1346"${data.date_year == "1346" ? " selected" : ""}>1346</option>
                                                                    <option value="1345"${data.date_year == "1345" ? " selected" : ""}>1345</option>
                                                                    <option value="1344"${data.date_year == "1344" ? " selected" : ""}>1344</option>
                                                                    <option value="1343"${data.date_year == "1343" ? " selected" : ""}>1343</option>
                                                                    <option value="1342"${data.date_year == "1342" ? " selected" : ""}>1342</option>
                                                                    <option value="1341"${data.date_year == "1341" ? " selected" : ""}>1341</option>
                                                                    <option value="1340"${data.date_year == "1340" ? " selected" : ""}>1340</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label">توضیحات</label>
                                                                    <textarea id="description" class="form-control col-xl-12"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>`
        }
    }
