<?php

namespace Modules\PermissionManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Support\Renderable;

class RoleController extends Controller
{

    public function index() {
        return response()->json([
            'role' => Role::get(),
        ]);
    }


    public function show($id)
    {
        $role = Role::with('permissions')->find($id);
        return response()->json([
            'role' => $role
        ]);
    }


   public function storeRole(Request $request) {

    $validator = Validator::make($request->all(), [
        'role_name' => 'required|string|unique:roles,name',
        'permission_ids' => 'array',
        'permission_ids.*' => 'exists:permissions,id',
    ]);

    if ($validator->fails()) {
        return response()->json(['errors' => $validator->errors()], 422);
    }

    $roleName = $request->input('role_name');
    $permissionIds = $request->input('permission_ids', []);

    $role = Role::create(['name' => $roleName , 'guard_name' => 'web']);

    if (!empty($permissionIds)) {
        $permissions = Permission::whereIn('id', $permissionIds)->get();
        $role->syncPermissions($permissions);
    }

    return response()->json(['message' => 'Role created successfully']);
   }



   public function updateRole($id, Request $request) {
    $validator = Validator::make($request->all(), [
        'role_name' => 'required|string|unique:roles,name,' . $id,
        'permission_ids_to_add' => 'array',
        'permission_ids_to_add.*' => 'exists:permissions,id',
        'permissions_to_remove' => 'array',
        'permissions_to_remove.*' => 'exists:permissions,id',
    ]);

    if ($validator->fails()) {
        return response()->json(['errors' => $validator->errors()], 422);
    }

    $role = Role::findOrFail($id);

    $roleName = $request->input('role_name');
    $permissionIdsToAdd = $request->input('permission_ids_to_add', []);
    $permissionsToRemove = $request->input('permissions_to_remove', []);

    $role->update(['name' => $roleName]);

    if (!empty($permissionIdsToAdd)) {
        $permissionsToAdd = Permission::whereIn('id', $permissionIdsToAdd)->get();
        $role->givePermissionTo($permissionsToAdd);
    }

    if (!empty($permissionsToRemove)) {
        $permissionsToRemove = Permission::whereIn('id', $permissionsToRemove)->get();
        $role->revokePermissionTo($permissionsToRemove);
    }

    return response()->json(['message' => 'Role updated successfully']);
}



   public function destroy($id) {
    Role::destroy($id);
    return response()->json([
        'message' => 'role deleted'
    ]);
   }

}
