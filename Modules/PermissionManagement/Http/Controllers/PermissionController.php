<?php

namespace Modules\PermissionManagement\Http\Controllers;

use Illuminate\Http\Request;
use Modules\User\Entities\User;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function permissionList()
    {
        $permissions = Permission::all();

        return response()->json(['permissions' => $permissions]);
    }

    
    public function addPermissionToUser(User $user , Request $request)
    {
        $permissionIdsToAdd = $request->input('permissions_to_add', []); // Array of permission IDs to add
        $permissionIdsToRemove = $request->input('permissions_to_remove', []); // Array of permission IDs to remove

        $permissionsToAdd = Permission::whereIn('id', $permissionIdsToAdd)->get();
        $permissionsToRemove = Permission::whereIn('id', $permissionIdsToRemove)->get();

        if ($permissionsToAdd->isEmpty() && $permissionsToRemove->isEmpty()) {
            return response()->json(['error' => 'No permissions to add or remove'], 400);
        }

        if (!$permissionsToAdd->isEmpty()) {
            $user->givePermissionTo($permissionsToAdd);
        }

        if (!$permissionsToRemove->isEmpty()) {
            $user->revokePermissionTo($permissionsToRemove);
        }

        return response()->json(['message' => 'Permissions updated for user']);
    }


    public function showRolesAndPermissions(User $user)
    {
        $roles = $user->getRoleNames();
        $permissions = $user->getAllPermissions()->pluck('name');

        return response()->json([
            'roles' => $roles,
            'permissions' => $permissions
        ]);
    }
}
