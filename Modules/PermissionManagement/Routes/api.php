<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\PermissionManagement\Http\Controllers\RoleController;
use Modules\PermissionManagement\Http\Controllers\PermissionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum','role:SuperAdmin']], function () {

Route::get('/permission/list',[PermissionController::class, 'permissionList']);
Route::post('/permission/add_user/{user}',[PermissionController::class, 'addPermissionToUser']);
Route::get('/user/role_permission/list/{user}',[PermissionController::class, 'showRolesAndPermissions']);


Route::post('/store/role',[RoleController::class, 'storeRole']);
Route::put('/update/role/{id}',[RoleController::class, 'updateRole']);
Route::delete('/delete/role/{id}',[RoleController::class, 'destroy']);
Route::get('/show/role/{id}',[RoleController::class, 'show']);
Route::get('/index/role',[RoleController::class, 'index']);

});
