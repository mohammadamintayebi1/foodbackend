<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Guest\Http\Controllers\GuestController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/check_food', [GuestController::class, 'checkFood']);
    Route::post('/test_auth', [GuestController::class, 'testAuth']);
});
