<?php

namespace Modules\Guest\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Guest\Entities\Guest;

class GuestController extends Controller
{
    public function checkFood(Request $request)
    {
        #todo validation --> id or code?
        $guest = Guest::find($request->guest_id);

        if (!$guest) {
            return response()->json(['message' => "کاربر یافت نشد"], 422);
        }
        if ($guest->available_food == 1) {
            $guest->available_food = 0;
            $guest->update();
            return response()->json(['message' => $guest->full_name . " غذا رزرو دارد "], 200);
        } else {
            return response()->json(['message' => $guest->full_name . " قبلا غذا را دریافت کرده "], 422);
        }
    }

    public function testAuth(Request $request)
    {

        return response()->json(['status' => "1"],);
    }
}
