<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Modules\Resume\Entities\Resume;
use Modules\User\Http\Controllers\UserController;

//??

Route::get('/dashboard', function () {
    $status = 0;
    if (Resume::where('user_id', Auth::user()->id)->first()) {
        $resume = Resume::where('user_id', Auth::user()->id)->first();
        $status = $resume->status;
    }
    return view('dashboard', ["status" => $status]);
})->name('dashboard')->middleware('auth');




//Route::get('/newUser', function () {
//    return view('user::newUser');
//})->name('newUser')->middleware('auth');
