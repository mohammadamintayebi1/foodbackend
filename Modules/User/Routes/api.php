<?php


use Illuminate\Support\Facades\Route;
use Modules\Resume\Entities\Resume;
use Modules\User\Entities\User;
use Modules\User\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function () {

    Route::get('/roleNames', [UserController::class, 'getRoleNames']);
    Route::post('/addUser', [UserController::class, 'addUser'])->name('addUser')->middleware('permission:user.store');
    Route::post('/editUser/{id}', [UserController::class, 'editUser'])->name('editUser')->middleware('permission:user.edit');

    Route::get('/singleUser/{id}', [UserController::class, 'index'])->name('singleUser')->middleware('permission:user.list');
    Route::delete('/deleteUser/{id}', [UserController::class, 'deleteUser'])->name('deleteUser')->middleware('permission:user.delete');
    Route::get('/allUsers', [UserController::class, 'index'])->name('allUsers')->middleware('permission:user.list');
    Route::get('/allUsers/none/paginate', [UserController::class, 'indexNonePaginate'])->name('allUsers')->middleware('permission:user.list');

});
