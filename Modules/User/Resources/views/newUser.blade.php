@include('layouts.header')
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">افزودن کاربر جدید</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="db-default.html"><i class="ti ti-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">داشبورد</a></li>
                        <li class="breadcrumb-item active">مدیریت کاربران</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <div class="row">
        <div class="col-xl-12">
            <!-- Hover -->
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>افزودن کاربر جدید</h4>
                </div>
                <div class="widget-body">
                    <form class="form-horizontal" method="post">
                        @csrf
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">نام *</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" id="first_name">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">نام خانوادگی *</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" id="last_name">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">موبایل *</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" id="phone_number">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">ایمیل</label>
                            <div class="col-lg-4">
                                <input type="email" class="form-control" id="email">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">انتخاب نقش</label>
                            <div class="col-lg-4">
                                <select id="role" class="custom-select form-control">
                                    <option value="SuperAdmin">مدیر کل</option>
                                    <option value="Univercity">مسئول دانشگاه</option>
                                    <option value="Customer" selected>کاربر</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">ارسال sms ثبت نام</label>
                            <div class="col-lg-4">
                                <input type="checkbox" class="form-control" id="sms">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <div class="col-lg-4">
                                <button type="button" class="btn btn-success mr-1 mb-2" id="send">افزودن</button>
                            </div>
                        </div>
                </div>
            </div>
            <!-- End Hover -->
        </div>
    </div>

</div>
<!-- End Row -->
<!-- End Container -->
@include('layouts.footer')
<script>
    $("#send").on('click', function () {
        $("#send").prop('disabled', true)
        $.ajax({
            url: '{{ route('addUser') }}',
            type: 'post',
            data: {
                _token: '{{ csrf_token() }}',
                first_name: $("#first_name").val(),
                last_name: $("#last_name").val(),
                phone_number: $("#phone_number").val(),
                email: $("#email").val(),
                role: $("#role").val(),
                sms: $('.myCheckbox').is(':checked') ? true : false
            },
            success: function (response) {
                alert('ثبت نام کاربر جدید با موفقیت انجام شد')
                location.href = '{{ route('allUsers') }}'
            },
            statusCode: {
                422: function (response) {
                    let object = response.responseJSON.errors
                    let text = "لطفا موارد زیر را بررسی نمایید:\n\n"
                    let i = 1
                    for (item in object) {
                        text += i + '. ' + object[item] + "\n"
                        i++
                    }
                    alert(text);
                    $("#send").prop('disabled', false)
                }
            }
        });
    });
</script>
