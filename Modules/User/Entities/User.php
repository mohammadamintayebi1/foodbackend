<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;
use Modules\Course\Entities\CourseItem;
use Modules\Course\Entities\CourseItemUser;
use Modules\Event\Entities\Event;
use Modules\Course\Entities\Course;
use Modules\PresentationVideo\Entities\Presentation;
use Modules\Resume\Entities\Resume;
use Modules\Ticket\Entities\Message;
use Modules\Ticket\Entities\Ticket;
use Modules\Test\Entities\TestResult;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Modules\University\Entities\University;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Certificate\Entities\Certificate;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
    use SoftDeletes;
    use LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'phone_number',
        'position',
        'company_name',
        'password',
        'other',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'other' => 'object'
    ];


    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }


    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['first_name', 'last_name', 'phone_number', 'email', 'password', 'created_at'])
            ->useLogName('کاربران')
            ->dontLogIfAttributesChangedOnly(['deleted_at'])
            ->logOnlyDirty();

    }

    public static function boot()
    {
        parent::boot();
    }
}
