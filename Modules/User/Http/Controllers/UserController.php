<?php

namespace Modules\User\Http\Controllers;

use App\Helpers\Vira;
use App\Exports\UsersExport;
use App\Http\Resources\UserCreationColloction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Resume\Entities\Resume;
use Modules\User\Entities\User;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function getRoleNames()
    {
        return Role::all()->map(function ($role) {
            return [
                'label' => $role->name,
                'value' => $role->id,
            ];
        });
    }
    /**
     * @OA\Get(
     *     path="/api/allUsers",
     *     description="show all users",
     *     summary="update user",
     *     tags={"User"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *    security={{"bearer_token":{}}},
     * )
     *
     *
     *
     * @OA\Get(
     *     path="/api/singleUser/{id}",
     *     description="single user data",
     *     summary="user data",
     *     tags={"User"},
     * @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="required user_id",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *    security={{"bearer_token":{}}},
     * )
     */
    public function index($id = null)
    {
        if (auth()->user()->hasPermissionTo('user.list')) {
            $users = User::orderBy('created_at', 'DESC');
        }

        if (request('search')) {
            $users = $users->whereRaw("CONCAT(COALESCE(first_name,''),' ',COALESCE(last_name,''),' ',COALESCE(phone_number,'')) like '%" . request('search') . "%'");
        }

        if ($id) {
            $user = User::with('roles',)->find($id);

            if (!$user) {
                return response()->json(['error' => 'User not found'], 404);
            }

            return response()->json([
                'user' => $user,
            ]);
        } else {
            $userCollection = $users->with('roles')->orderBy('created_at', 'asc')->get();

            $indexedUserCollection = $userCollection->map(function ($user, $index) {
                $user['row_num'] = $index + 1; // Add index to each user
                return $user;
            });


            $totalUserNumber = User::count();
            return response()->json([
                'users' => $indexedUserCollection,
                'total' => $totalUserNumber,
            ]);
        }
    }

    public function indexNonePaginate($id = null)
    {
        if (auth()->user()->hasPermissionTo('SuperAdmin')) {
            $users = User::orderBy('created_at', 'DESC');
        } else {
            $users = auth()->user()->customer()->orderBy('created_at', 'DESC');
        }

        if (request('search')) {
            $users = $users->whereRaw("CONCAT(COALESCE(first_name,''),' ',COALESCE(last_name,''),' ',COALESCE(phone_number,''),' ',COALESCE(email,'')) like '%" . request('search') . "%'");
        }

        if ($id) {
            $user = User::with('roles')->find($id);

            if (!$user) {
                return response()->json(['error' => 'User not found'], 404);
            }

            return response()->json([
                'user' => $user,
            ]);
        } else {
            $userCollection = $users->with('roles')->orderBy('created_at', 'asc')->get();

            $indexedUserCollection = $userCollection->map(function ($user, $index) {
                $user['row_num'] = $index + 1; // Add index to each user
                return $user;
            });


            return response()->json([
                'users' => $indexedUserCollection,
            ]);
        }
    }


    public function editUser(UserRequest $request, $id)
    {
        $user = User::find($id);
        $user->update($request->merge([
            'password' => $request->password ? Hash::make($request->password) : $user->password
        ])->toArray());


        if ($request->role) {
            $user->roles()->detach();
            $user->assignRole($request->role);
        }
        return response()->json('ok');
    }

    /**
     * @OA\Delete(
     *     path="/api/deleteUser/{id}",
     *     description="delete user",
     *     summary="delete user",
     *     tags={"User"},
     * @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="required user_id",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *    security={{"bearer_token":{}}},
     * )
     */
    public function deleteUser($id)
    {
        User::find($id)->delete();

        return response()->json([
            'message' => "user with id of $id deleted"
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/addUser",
     *     tags={"User"},
     *    security={{"bearer_token":{}}},
     *     summary="add user",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="phone_number",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="role",
     *                     type="string"
     *                 ),
     *                    @OA\Property(
     *                     property="first_name",
     *                     type="string"
     *                 ),
     *                    @OA\Property(
     *                     property="last_name",
     *                     type="string"
     *                 ),
     *                     @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *  @OA\Response(response="default", description="login")
     * )
     */

    public function addUser(UserRequest $request)
    {
        $user = User::where('phone_number', $request->phone_number)->first();
        if ($user) {
            if ($user->other->status == 0) {
                $user->forceDelete();
            } else {
                return response()->json(['message' => 'موبایل وارد شده تکراری می‌باشد'], 403);
            }
        }
        $password = $request->password;
        $user = User::create($request->merge([
            'password' => Hash::make($password),
            'phone_number' => Vira::fa2en($request->phone_number),
            'other' => ['code' => 'ثبت نام شده توسط مدیریت', 'status' => 1]
        ])->toArray());

        $user->assignRole($request->role);

        if ($request->sms === true) {
            $input_data = [
                "name" => $user->first_name . ' ' . $user->last_name,
                "username" => $user->phone_number,
                "password" => $password
            ];
            $smsPattern = config('sms.patterns.admin_add_user');
            Vira::send_sms($input_data, $smsPattern, $user->phone_number);
        }
        return response()->json('ok');
    }


}
