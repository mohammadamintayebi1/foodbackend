<?php

namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required' , 'persian_alpha'],
            'last_name' => ['required' , 'persian_alpha'],
            'phone_number' => ['unique:users,phone_number','required','numeric'],
            'password' => ['required' , 'min:8'],
            'checkbox' => ['required']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
