<?php

namespace Modules\Auth\Http\Controllers\API;

use http\Env\Response;
use Illuminate\Http\Request;
use Modules\User\Entities\User;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/auth/login",
     *     tags={"auth"},
     *
     *     summary="login",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="phone_number",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *  @OA\Response(response="default", description="login")
     * )
     */


    public function loginUser(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(),
                [
                    'phone_number' => 'required',
                    'password' => 'required'
                ]);

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 403);
            }

            if (!Auth::attempt($request->only(['phone_number', 'password']))) {
                return response()->json([
                    'status' => false,
                    'message' => 'Email & Password does not match with our record.',
                ], 403);
            }

            $user = User::where('phone_number', $request->phone_number)->with('permissions')->first();
            if ($user->other->status == 0) {
                $user->forceDelete();
                return response()->json([
                    'status' => false,
                    'message' => 'users status was 0 need confirmation'
                ], 403);
            } else {

                activity('ورود')
                    ->causedBy($user)
                    ->event('ورود کاربر')
                    ->log('ورود کاربر');


                return response()->json([
                    'status' => true,
                    'message' => 'User Logged In Successfully',
                    'token' => $user->createToken("API TOKEN")->plainTextToken,
                    'user' => $user,
                    'user_role' => $user->getRoleNames(),
                    'user_permissions' => $user->roles()->get()[0]->permissions->pluck('name'), //todo if user has multi roles we have errors
                ], 200);

            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function loginAsUser($user)
    {
        $user = User::with('permissions')->find($user);
        if (!$user) {
            return response()->json([
                'message' => "cant find user with id of $user"
            ]);
        } else {
            activity('ورود')
                ->causedBy(Auth::user())
                ->performedOn($user)
                ->event('ورود به عنوان کاربر دیگر')
                ->log('ورود به عنوان کاربر دیگر');

            return response()->json([
                'status' => true,
                'message' => 'User Logged In Successfully',
                'user' => $user,
                'token' => $user->createToken("API TOKEN")->plainTextToken,
                'user_role' => $user->getRoleNames(),
                'user_permissions' => $user->getAllPermissions()->pluck('name'),

            ], 200);
        }
    }

}
