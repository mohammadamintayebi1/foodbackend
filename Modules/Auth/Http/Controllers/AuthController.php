<?php

namespace Modules\Auth\Http\Controllers;

use App\Helpers\Vira;
use Illuminate\Http\Request;
use Modules\Resume\Entities\Resume;
use Modules\User\Entities\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Auth\Http\Requests\EditProfile;
use Modules\Auth\Http\Requests\LoginRequest;
use Modules\Auth\Http\Requests\RegisterRequest;
use Modules\Auth\Http\Requests\ResetPasswordRequest;

class AuthController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/sayHi",
     *     summary="Say Hi",
     *     tags={"hi"},
     *     @OA\Response(response="default", description="sayHi"),
     *   security={{"bearer_token":{}}},
     * )
     */
    public function sayHi()
    {
        return response()->json(['message' => 'Hi']);
    }

    /**
     * @OA\Post(
     *     path="/api/register",
     *     description="Register: Creates a new user with the provided phone number and sends a verification code via SMS.",
     *     summary="User Registration",
     *     tags={"auth"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Request body for user registration.",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"phone_number", "password","first_name"},
     *                     @OA\Property(
     *                     property="checkbox",
     *                     type="string",
     *                     example=1,
     *                     description="User's password."
     *                 ),
     *                     @OA\Property(
     *                     property="first_name",
     *                     type="string",
     *                     description="User's password."
     *                 ),
     *                     @OA\Property(
     *                     property="last_name",
     *                     type="string",
     *                     description="User's password."
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     type="string",
     *                     example="1234567890",
     *                     description="User's phone number."
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string",
     *                     example="secretpassword",
     *                     description="User's password."
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK. User registered successfully.",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 type="string",
     *                 example="User registered successfully"
     *             ),
     *             @OA\Property(
     *                 property="phone_number",
     *                 type="string",
     *                 example="1234567890",
     *                 description="Registered user's phone number."
     *             ),
     *             @OA\Property(
     *                 property="password",
     *                 type="string",
     *                 example="secretpassword",
     *                 description="Registered user's password."
     *             ),
     *             @OA\Property(
     *                 property="code",
     *                 type="integer",
     *                 example=1234,
     *                 description="Verification code sent to the user's phone number."
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request. Duplicate phone number.",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="string",
     *                 example="Duplicate phone number"
     *             )
     *         )
     *     ),
     * )
     */
    public function register(RegisterRequest $request)
    {
        $user = User::where('phone_number', $request->phone_number)->first();

        if ($user) {
            if ($user->other->status == 0) {
                $user->forceDelete();
                return response()->json(['message' => 'User deleted due to inactive status'], 200);
            } else {
                return response()->json(['error' => 'Duplicate phone number'], 400);
            }
        }

        $code = rand(1000, 9999);
        $password = $request->password;
        $newUser = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'password' => Hash::make($request->password),
            'phone_number' => Vira::fa2en($request->phone_number),
            'other' => ['code' => $code, 'status' => 0]
        ]);

        $input_data = ["code" => $code];
        $smsPattern = config('sms.patterns.otp');
        Vira::send_sms($input_data, $smsPattern, $newUser->phone_number);

        return response()->json([
            'message' => 'User registered successfully',
            'phone_number' => $newUser->phone_number,
        ], 200);
    }

    /**
     * @OA\Post(
     *     path="/api/editProfile",
     *     description="editProfile",
     *     summary="editProfile",
     *   security={{"bearer_token":{}}},
     *     tags={"auth"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="reset password",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"first_name", "last_name"},
     *                     @OA\Property(
     *                     property="first_name",
     *                     type="string",
     *                 ),
     *                     @OA\Property(
     *                     property="last_name",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="email",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="phone_number",
     *                     type="string",
     *                 ),
     *             )
     *         )
     *     ),
     *    @OA\Response(response="default", description="edituser"),
     * )
     */
    public function editProfile(EditProfile $request)
    {
        $user = Auth::user();
        $user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,

        ]);

        return response()->json([
            'message' => 'profile updated'
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/resetPassword",
     *     description="reset password",
     *     summary="reset password",
     *   security={{"bearer_token":{}}},
     *     tags={"auth"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="reset password",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"old_pass", "new_pass"},
     *                     @OA\Property(
     *                     property="old_pass",
     *                     type="string",
     *                     description="User's password."
     *                 ),
     *                     @OA\Property(
     *                     property="new_pass",
     *                     type="string",
     *                     description="User's password."
     *                 ),
     *             )
     *         )
     *     ),
     *    @OA\Response(response="default", description="resetPassword"),
     * )
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $user = auth()->user();
        $credentials = [
            "phone_number" => $user->phone_number,
            "password" => $request->old_pass,
        ];

        if (Hash::check($request->old_pass, $user->password)) {
            $user->password = Hash::make($request->new_pass);
            $user->save();

            return response()->json([
                'message' => 'Password updated'
            ]);
        } else {
            return response()->json([
                'message' => 'Incorrect password'
            ], 406); // Unauthorized status code
        }
    }

    //    public function login(LoginRequest $request)
    //    {
    //        $credentials = [
    //            "phone_number" => Vira::fa2en($request->phone_number),
    //            "password" => $request->password,
    //        ];
    //        if (Auth::attempt($credentials)) {
    //            if (User::where('phone_number', Vira::fa2en($request->phone_number))->where('other->status', 0)->exists()) {
    //                User::where('phone_number', Vira::fa2en($request->phone_number))->delete();
    //                echo '<script>alert("کاربری با این مشخصات یافت نشد");window.location.href = "/login";</script>';
    //                die();
    //            }
    //            $request->session()->regenerate();
    //            return redirect()->route('dashboard');
    //        }
    //        echo '<script>alert("اطلاعات وارد شده صحیح نمی‌باشد.");window.location.href = "/login";</script>';
    //    }

    /**
     * @OA\post(
     *     path="/api/otp",
     *     summary="complete registration",
     *     tags={"auth"},
     * @OA\RequestBody(
     *         required=true,
     *         description="Request body for user registration.",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"phone_number", "again","code"},
     *                     @OA\Property(
     *                     property="again",
     *                     type="string",
     *                     example=0,
     *                     description="gets code again"
     *                 ),
     *                     @OA\Property(
     *                     property="phone_number",
     *                     type="string",
     *                     description="User's phone_number"
     *                 ),
     *                     @OA\Property(
     *                     property="code",
     *                     type="string",
     *                     description="User's sended code"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(response="default", description="user data")
     * )
     */
    public function otp(Request $request)
    {
        // Validate the incoming request data
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|string',
            'again' => 'required|boolean',
            'code' => 'required_if:again,false|integer|min:1000|max:9999',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => 'Validation failed', 'errors' => $validator->errors()], 422);
        }

        $phoneNumber = Vira::fa2en($request->phone_number);
        $user = User::where('phone_number', $phoneNumber)->first();

        if (!$user) {
            return response()->json(['error' => 'User not found'], 404);
        }

        if ($request->again == true) {
            // Generate a new OTP code
            $code = rand(1000, 9999);
            $user->forceFill(['other->code' => $code])->save();
            $input_data = ["code" => $code];
            $smsPattern = config('sms.patterns.otp');
            Vira::send_sms($input_data, $smsPattern, $user->phone_number);

            return response()->json(['message' => 'OTP code sent successfully']);
        }

        if ($user->other->code == $request->code) {
            // Update the user status and assign the role
            $user->forceFill(['other->status' => 1])->save();
            $user->assignRole('Customer');

            // Log in the user
            $token = $user->createToken('auth-token')->plainTextToken;

            return response()->json(['message' => 'OTP code verified successfully', 'user' => $user, 'token' => $token,
                'user_role' => $user->getRoleNames(),
                'user_permissions' => $user->getAllPermissions()->pluck('name'),
            ]);

        }
        return response()->json(['error' => 'Invalid OTP code', 'message' => 'Login failed'], 422);
    }

    /**
     * @OA\post(
     *     path="/api/me",
     *     summary="get authed user data",
     *     tags={"auth"},
     *       security={{"bearer_token":{}}},
     *     @OA\Response(response="default", description="user data")
     * )
     */
    public function me(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $response = [
            'user' => $user,
            'user_role' => $user->getRoleNames(),
            'user_permissions' => $user->roles()->get()[0]->permissions->pluck('name'),

        ];


        return response()->json($response);
    }

    /**
     * @OA\Post(
     *     path="/api/logout",
     *     description="Logout: Invalidate the user's access tokens and log out the user.",
     *     summary="Logout",
     *     tags={"auth"},
     *     security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="OK. User logged out successfully.",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 type="string",
     *                 example="Logged out successfully"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized. Authentication failed or user not logged in.",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="string",
     *                 example="Unauthorized"
     *             )
     *         )
     *     ),
     * )
     */
    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return response()->json(['message' => 'Logged out successfully']);
    }

    /**
     * @OA\Post(
     *     path="/api/forgotPassword",
     *   description="Forgot password: Sends a new password via SMS to the user's phone number.",
     *     summary="Forgot Password",
     *     tags={"auth"},
     *  @OA\RequestBody(
     *         required=true,
     *         description="Request body for forgot password.",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"phone_number"},
     *                 @OA\Property(
     *                     property="phone_number",
     *                     type="string",
     *                     example="1234567890",
     *                     description="User's phone number."
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(response="default", description="forgetPassword"),
     * )
     */
    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $user = User::where('phone_number', $request->phone_number)->first();
        $password = rand(100000, 999999);

        if ($user) {

            if ($user->other->status == 0) {
                $user->delete();
                return response()->json([
                    'status' => false,
                    'message' => 'users status was 0 need confirmation'
                ], 401);
            } else {
                $user->password = Hash::make($password);
                $user->update();

                $input_data = [
                    "name" => $user->first_name . ' ' . $user->last_name,
                    "username" => $user->phone_number,
                    "password" => $password
                ];
                $smsPattern = config('sms.patterns.forget_password');
                Vira::send_sms($input_data, $smsPattern, $user->phone_number);

                return response()->json(['message' => 'New password sent via SMS'], 200);
            }
        }

        return response()->json(['error' => 'Mobile number not found'], 200);
    }
}
