<!DOCTYPE html>
<!--
Item Name: Adminice - Web App & Admin Dashboard Template
Version: 1.0
Author: jafar abbasi
-->
<html lang="fa">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>{{ $title }}</title>
    <meta name="description" content="Adminice is a Web App and Admin Dashboard Template built with Bootstrap 4" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Font Iran licence -->
    <link rel="stylesheet" href="assets/css/fontiran.css" />
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png?v=1" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png?v=1" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png?v=1" />
    <!-- Stylesheet -->
    <link rel="stylesheet" href="assets/vendors/css/base/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/vendors/css/base/seenboard-1.0.css" />
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
</head>

<body class="bg-white" onload="startTimer()">
    <!-- Begin Preloader -->
    <div id="preloader">
        <div class="canvas">
            <img src="assets/img/rahekar-FA.svg" alt="logo" class="loader-logo" />
            <div class="spinner"></div>
        </div>
    </div>
    <a class="btn btn-lg btn-gradient-01" href="{{ env('MAIN_SITE_URL') }}" style="background: #a26bdd; position:fixed; top: 25px; left: 25px;z-index: 10;">بازگشت به سایت</a>
    <!-- End Preloader -->
    <!-- Begin Container -->
    <div class="container-fluid no-padding h-100">
        <div class="row flex-row h-100 bg-white">
            <!-- Begin Left Content -->

            <!-- End Left Content -->
            <!-- Begin Right Content -->
            <div class="col-xl-4 col-lg-6 col-md-7 my-auto no-padding">
                <!-- Begin Form -->
                <div class="authentication-form mx-auto">
                    <div class="logo-centered">
                        <a href="/register">
                            <img src="assets/img/rahekar-FA.svg" alt="logo" />
                        </a>
                    </div>
                    <h3>کد تایید را وارد نمایید</h3>
                    <form method="post" action="{{ route('user.otp') }}">
                        @csrf
                        <div class="group material-input">
                            <div class="otp-timer" style="text-align:center">
                                <span class="text"> زمان باقیمانده :</span>
                                <span id="seconds">00</span>
                                <span class="dot">:</span>
                                <span id="minutes">0</span>
                            </div>
                        </div>
                        <div class="group material-input">
                            <input id="phone_number" type="text" required name="phone_number" value="{{ $phone_number }}" disabled style="text-align:center" />
                            <input id="phone_number2" type="hidden" name="phone_number" value="{{ $phone_number }}" />
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>موبایل</label>
                        </div>
                        <div class="group material-input">
                            <input id="code" type="number" required name="code" style="text-align: center; letter-spacing:10px" />
                            <input type="hidden" name="password" value="{{ $password }}">
                            <input type="hidden" name="again" id="again" value="0">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>کد تایید</label>
                        </div>
                        <div class="sign-btn text-center">
                            <button id="confirm" type="submit" class="btn btn-lg btn-gradient-01">
                                تایید
                            </button>
                        </div>
                    </form>

                </div>
                <!-- End Form -->
            </div>
            <!-- End Right Content -->
            <div class="col-xl-8 col-lg-6 col-md-5 no-padding">
                <div class="seenboard-bg background-01">
                    <!-- <div class="seenboard-overlay overlay-01"></div> -->
                    <!-- <div class="authentication-col-content mx-auto">
                    <h1 class="gradient-text-01">به سـین بورد خوش آمدید!</h1>
                    <span class="description">
                      پنل مدیریتی ادمین نایس کامل و زیبا برای پیاده سازی ایده هایتان
                      می توانید بهترین گزینه ها را ببنید و انتخاب کنید.
                    </span>
                  </div> -->
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- End Container -->
    <script>
        var minutes;
        var seconds;
        var set_inteval;

        function otp_timer() {
            seconds -= 1;
            document.getElementById('seconds').innerHTML = seconds;
            document.getElementById('minutes').innerHTML = minutes;
            if (seconds == 0) {
                if (minutes > 0) {
                    seconds = 60;
                    minutes -= 1;
                } else {
                    minutes = 0;
                    document.getElementById('minutes').innerHTML = minutes;
                    clearInterval(set_inteval);
                    minutes = 0;
                    seconds = 0;
                    document.getElementById('seconds').innerHTML = '00';
                    document.getElementById('minutes').innerHTML = '0';
                    document.getElementById("phone_number").disabled = false;
                    document.getElementById("phone_number2").disabled = true;
                    document.getElementById("code").disabled = true;
                    document.getElementById("confirm").innerHTML = 'ارسال مجدد';
                    document.getElementById("again").value = 1;
                }
            }
        }

        function startTimer() {
            minutes = 1;
            seconds = 60;
            document.getElementById('seconds').innerHTML = seconds;
            document.getElementById('minutes').innerHTML = minutes;
            set_inteval = setInterval('otp_timer()', 1000);
        }
    </script>
    <!-- Begin Vendor Js -->
    <script src="assets/vendors/js/base/jquery.min.js"></script>
    <script src="assets/vendors/js/base/core.min.js"></script>
    <!-- End Vendor Js -->
    <!-- Begin Page Vendor Js -->
    <script src="assets/vendors/js/app/app.min.js"></script>
    <!-- End Page Vendor Js -->
</body>

</html>
