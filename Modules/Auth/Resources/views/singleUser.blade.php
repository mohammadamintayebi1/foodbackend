@include('layouts.header')
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">ویرایش کاربر</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="db-default.html"><i class="ti ti-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">داشبورد</a></li>
                        <li class="breadcrumb-item active">مدیریت کاربران</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <div class="row">
        <div class="col-xl-12">
            <!-- Hover -->
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>فرم ویرایش</h4>
                </div>
                <div class="widget-body">
                    <form class="form-horizontal" method="post">
                        @csrf
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">نام *</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{ $user->first_name }}" id="first_name">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">نام خانوادگی *</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{ $user->last_name }}" id="last_name">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">موبایل *</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{ $user->phone_number }}"
                                       id="phone_number">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">ایمیل</label>
                            <div class="col-lg-4">
                                <input type="email" class="form-control" value="{{ $user->email }}" id="email">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">تغییر رمز عبور</label>
                            <div class="col-lg-4">
                                <input type="password" class="form-control" id="password"
                                       placeholder="در صورت عدم تغییر خالی بگذارید">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">انتخاب نقش</label>
                            <div class="col-lg-4">
                                <select id="role" class="custom-select form-control">
                                    <option
                                        value="SuperAdmin"{{ $user->getRoleNames()[0] == 'SuperAdmin' ? ' selected' : '' }}>
                                        مدیر کل
                                    </option>
                                    <option
                                        value="Univercity"{{ $user->getRoleNames()[0] == 'Univercity' ? ' selected' : '' }}>
                                        مسئول دانشگاه
                                    </option>
                                    <option
                                        value="Customer"{{ $user->getRoleNames()[0] == 'Customer' ? ' selected' : '' }}>
                                        کاربر
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">دانشگاه (ویژه بخش مدیریت دانشگاه ها)</label>
                            <div class="col-lg-4">
                                <input list="brow" class="form-control" style="background: none" id="university">
                                <datalist id="brow"></datalist>
                                <input type="hidden" name="answer" id="university-hidden">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <div class="col-lg-4">
                                <button type="button" class="btn btn-success mr-1 mb-2" id="send">بروزرسانی</button>
                            </div>
                        </div>
                </div>
            </div>
            <!-- End Hover -->
        </div>
    </div>

</div>
<!-- End Row -->
<!-- End Container -->
@include('resources.views.layouts.footer')
<script>
    document.querySelector('input[list]').addEventListener('input', function (e) {
        var input = e.target,
            list = input.getAttribute('list'),
            options = document.querySelectorAll('#' + list + ' option'),
            hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden'),
            inputValue = input.value;

        hiddenInput.value = inputValue;

        for (var i = 0; i < options.length; i++) {
            var option = options[i];

            if (option.value === inputValue) {
                hiddenInput.value = option.getAttribute('data-value');
                break;
            }
        }
    });
    $(window).on('load', function () {
        let selectValues = []
        $.ajax({
            type: "get",
            url: '/universities',
            success: function (result) {
                selectValues = result;
                selectValues.forEach(function (item) {
                    $('#brow')
                        .append($('<option></option>', {
                            value: item.name,
                            "data-value": item.id
                        }));
                });
            },
            error: function (jqXhr, textStatus, errorMessage) {
                console.log(errorMessage);
            },
            dataType: "json"
        });
    });


    $("#send").on('click', function () {
        $("#send").prop('disabled', true)
        $.ajax({
            url: '{{ route('editUser', ['id' => $user->id]) }}',
            type: 'post',
            data: {
                _token: '{{ csrf_token() }}',
                first_name: $("#first_name").val(),
                last_name: $("#last_name").val(),
                phone_number: $("#phone_number").val(),
                email: $("#email").val(),
                role: $("#role").val(),
                university: $("#university-hidden").val(),
                password: $("#password").val()
            },
            success: function (response) {
                $("#password").val('')
                alert('اطلاعات کاربر با موفقیت ویرایش شد')
                $("#send").prop('disabled', false)
            },
            statusCode: {
                422: function (response) {
                    let object = response.responseJSON.errors
                    let text = "لطفا موارد زیر را بررسی نمایید:\n\n"
                    let i = 1
                    for (item in object) {
                        text += i + '. ' + object[item] + "\n"
                        i++
                    }
                    alert(text);
                    $("#send").prop('disabled', false)
                }
            }
        });
    });
</script>
