<!DOCTYPE html>
<!--
Item Name: Adminice - Web App & Admin Dashboard Template
Version: 1.0
Author: jafar abbasi
-->
<html lang="fa">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>{{ $title }}</title>
    <meta name="description" content="Adminice is a Web App and Admin Dashboard Template built with Bootstrap 4" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Font Iran licence -->
    <link rel="stylesheet" href="assets/css/fontiran.css" />
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png?v=1" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png?v=1" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png?v=1" />
    <!-- Stylesheet -->
    <link rel="stylesheet" href="assets/vendors/css/base/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/vendors/css/base/seenboard-1.0.css" />
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
</head>

<body class="bg-white">
    <!-- Begin Preloader -->
    <div id="preloader">
        <div class="canvas">
            <img src="assets/img/rahekar-FA.svg" alt="logo" class="loader-logo" />
            <div class="spinner"></div>
        </div>
    </div>
    <a class="btn btn-lg btn-gradient-01" href="{{ env('MAIN_SITE_URL') }}" style="background: #a26bdd; position:fixed; top: 25px; left: 25px;z-index: 10;">بازگشت به سایت</a>
    <!-- End Preloader -->
    <!-- Begin Container -->
    <div class="container-fluid no-padding h-100">
        <div class="row flex-row h-100 bg-white">
            <!-- Begin Left Content -->

            <!-- End Left Content -->
            <!-- Begin Right Content -->
            <div class="col-xl-4 col-lg-6 col-md-7 my-auto no-padding">
                <!-- Begin Form -->
                <div class="authentication-form mx-auto">
                    <h1 style="margin-bottom: 50px; font-size: 34px; line-height: 4rem;text-align:center">طرح ملی ترویج کارآفرینی</h1>
                    <h3>ثبت نام در راهه‌کار</h3>
                    <form method="post" action="{{ route('user.register') }}">
                        @csrf
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="group material-input">
                            <input type="text" required name="first_name" />
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>نام</label>
                        </div>
                        <div class="group material-input">
                            <input type="text" required name="last_name" />
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>نام خانوادگی</label>
                        </div>
                        <div class="group material-input">
                            <input type="text" required name="phone_number" />
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>موبایل</label>
                        </div>
                        <div class="group material-input">
                            <input type="password" required name="password" />
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>رمز عبور</label>
                        </div>

                        <div class="row">
                            <div class="col text-left">
                                <div class="styled-checkbox">
                                    <input type="checkbox" name="checkbox" id="agree" onchange="rules()" />
                                    <label for="agree">من <a href="{{ env('terms_and_conditions') }}" target="_blank">قوانین و مقررات </a> را قبول می
                                        کنم</label>
                                </div>
                            </div>
                        </div>
                        <div class="sign-btn text-center">
                            <button type="submit" class="btn btn-lg btn-gradient-01" id="sendbtn" disabled>
                                ثبت نام
                            </button>
                            <a class="btn btn-lg btn-gradient-01" href="{{ route('login') }}" style="background: none;border: solid 1px #a26bdd;color: #a26bdd;">ورود</a>
                        </div>
                    </form>
                </div>
                <div class="logo-centered mt-5">
                    <a href="/login">
                        <img src="assets/img/rahekar-FA.svg" alt="logo" />
                    </a>
                </div>
                <!-- End Form -->
            </div>
            <!-- End Right Content -->
            <div class="col-xl-8 col-lg-6 col-md-5 no-padding">
                <div class="seenboard-bg background-01">
                    <!-- <div class="seenboard-overlay overlay-01"></div> -->
                    <!-- <div class="authentication-col-content mx-auto">
                    <h1 class="gradient-text-01">به سـین بورد خوش آمدید!</h1>
                    <span class="description">
                      پنل مدیریتی ادمین نایس کامل و زیبا برای پیاده سازی ایده هایتان
                      می توانید بهترین گزینه ها را ببنید و انتخاب کنید.
                    </span>
                  </div> -->
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- End Container -->
    <!-- Begin Vendor Js -->
    <script src="assets/vendors/js/base/jquery.min.js"></script>
    <script src="assets/vendors/js/base/core.min.js"></script>
    <!-- End Vendor Js -->
    <!-- Begin Page Vendor Js -->
    <script src="assets/vendors/js/app/app.min.js"></script>
    <!-- End Page Vendor Js -->
    <script>
        $("#agree").change(function() {
            if ($('#agree').is(":checked")) {
                $('#sendbtn').prop('disabled', false);
            } else {
                $('#sendbtn').prop('disabled', true);
            }
        });
    </script>
</body>

</html>
