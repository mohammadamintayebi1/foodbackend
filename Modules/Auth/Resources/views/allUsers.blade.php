@include('layouts.header')
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">مدیریت کاربران</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="db-default.html"><i class="ti ti-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">داشبورد</a></li>
                        <li class="breadcrumb-item active">تیکت های من</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <div class="row">
        <div class="col-xl-12">
            <!-- Hover -->

            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions align-items-center">
                    <div class="row">
                        @can('user.store')
                            <div class="col-lg-2"><a href="{{ route('newUser') }}"
                                                     class="btn btn-gradient-01 mr-1 mb-2">ایجاد کاربر جدید</a></div>
                        @endcan
                        @can('user.list')
                            <div class="col-lg-6">
                                <form action="" method="get">
                                    <div class="row">
                                        <div class="col-lg-8" style="padding-right:30px"><input type="text"
                                                                                                class="form-control"
                                                                                                value="{{ request('search') }}"
                                                                                                name="search"
                                                                                                placeholder="جستجوی کاربر ... (نام، نام خانوادگی، موبایل، ایمیل)">
                                        </div>
                                        <div class="col-lg-4">
                                            <button type="submit" class="btn btn-secondary btn-square mr-1 mb-2">جستجو
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endcan
                    </div>
                </div>
                <div class="widget-body">
                    @if (isset($message))
                        <div class="alert alert-{{ $message->status }} alert-dissmissible fade show mt-2 mb-4"
                             role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                            {{ $message->text }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th>شماره</th>
                                <th>نام</th>
                                <th>نام خانوادگی</th>
                                <th>موبایل</th>
                                <th>ایمیل</th>
                                <th><span style="width:100px;">نقش</span></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td><span class="text-primary">{{ $user->id }}</span></td>
                                    <td>{{ $user->first_name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td>{{ $user->phone_number }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @switch($user->getRoleNames()[0])
                                            @case('SuperAdmin')
                                                <span class="badge-text badge-text success">
                                                        مدیر کل
                                                    </span>
                                                @break

                                            @case('Univercity')
                                                <span class="badge-text badge-text primary">
                                                        مسئول دانشگاه
                                                    </span>
                                                @break

                                            @case('Customer')
                                                <span class="badge-text badge-text info">
                                                        کاربر
                                                    </span>
                                                @break

                                                </span>
                                        @endswitch
                                    </td>
                                    <td class="td-actions">
                                        @can('ticket.store')
                                            <a href="{{ route('newTicket', ['userId' => $user->id]) }}"
                                               class="btn btn-outline-info ripple mr-1 mb-2">ارسال تیکت</a>
                                        @endcan
                                        @can('user.edit')
                                            <a href="{{ route('singleUser', ['id' => $user->id]) }}"
                                               class="btn btn-outline-success ripple mr-1 mb-2">ویرایش</a>
                                        @endcan
                                        @can('user.delete')
                                            <a href="{{ route('deleteUser', ['id' => $user->id]) }}"
                                               class="btn btn-outline-danger ripple mr-1 mb-2"
                                               onclick="return confirm('آیا از حذف این کاربر اطمینان دارید؟\nپس از حذف تمام سوابق مربوط به رزومه، و تیکتها نیز حذف خواهد شد!');">حذف
                                                کاربر</a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
            <!-- End Hover -->
        </div>
    </div>
</div>
<!-- End Row -->
<!-- End Container -->
@include('layouts.footer')
