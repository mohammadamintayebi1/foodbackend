@include('layouts.header')
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">تغییر رمز عبور</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="db-default.html"><i class="ti ti-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">داشبورد</a></li>
                        <li class="breadcrumb-item active">تغییر رمز عبور</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Begin Row -->
    <div class="row flex-row">
        <div class="col-12">
            <!-- Form -->
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>تغییر رمز عبور</h4>
                </div>
                <div class="widget-body">
                    <form class="form-horizontal" action="{{ route('user.resetPassword') }}" method="post">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">رمز عبور فعلی</label>
                            <div class="col-lg-4">
                                <input type="password" class="form-control" name="old_pass" required>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">رمز عبور جدید</label>
                            <div class="col-lg-4">
                                <input type="password" class="form-control" name="new_pass" id="new_pass" required>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">تکرار رمز عبور جدید</label>
                            <div class="col-lg-4">
                                <input type="password" class="form-control" id="new_pass_2" onkeyup="checkpass()"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-success mr-1 mb-2" id="update" disabled>تغییر
                                    رمز
                                </button>
                            </div>
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- End Form -->
</div>
</div>
<script>
    function checkpass() {
        let pass_1 = document.querySelector("#new_pass");
        let pass_2 = document.querySelector("#new_pass_2");
        let update = document.querySelector("#update");

        if (pass_1.value != pass_2.value) {
            pass_2.style.borderColor = "red"
            update.disabled = true
        } else {
            pass_2.style.borderColor = "green"
            update.disabled = false
        }
    }
</script>
<!-- End Row -->
</div>
<!-- End Row -->
<!-- End Container -->
@include('layouts.footer')
