@include('layouts.header')
<?php
$user = auth()->user();
?>
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">ویرایش پروفایل</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="db-default.html"><i class="ti ti-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">داشبورد</a></li>
                        <li class="breadcrumb-item active">ویرایش پروفایل</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Begin Row -->
    <div class="row flex-row">
        <div class="col-12">
            <!-- Form -->
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>ویرایش پروفایل</h4>
                </div>
                <div class="widget-body">
                    <form class="form-horizontal" method="post" action="{{ route('user.editProfile') }}">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">نام</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{ $user->first_name }}"
                                       name="first_name">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">نام خانوادگی</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{ $user->last_name }}" name="last_name">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">موبایل</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{ $user->phone_number }}"
                                       name="phone_number">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label">ایمیل</label>
                            <div class="col-lg-4">
                                <input type="email" class="form-control" value="{{ $user->email }}" name="email">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-success mr-1 mb-2">بروزرسانی</button>
                            </div>
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- End Form -->
</div>
</div>
<!-- End Row -->
</div>
<!-- End Row -->
<!-- End Container -->
@include('layouts.footer')
