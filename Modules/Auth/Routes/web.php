<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;
use Modules\Auth\Http\Controllers\AuthController;

Route::get('/login', function () {
   return view('auth::login', ['title' => 'راهه کار | ورود']);
})->name('login');

Route::get('/register', function () {
   return view('auth::register', ['title' => 'راهه کار | ثبت نام']);
})->name('register');

Route::get('/forgotPassword', function () {
   return view('auth::forgotPassword', ['title' => 'راهه کار | بازیابی رمز عبور']);
})->name('forgotPassword');

Route::get('/resetPassword', function () {
   return view('auth::resetPassword');
})->name('resetPassword')->middleware('auth');



Route::get('/editProfile', function () {
   return view('auth::editProfile');
})->name('editProfile')->middleware('auth');




