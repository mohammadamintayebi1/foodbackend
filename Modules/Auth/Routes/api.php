<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Auth\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('/auth/login', [\Modules\Auth\Http\Controllers\API\AuthController::class, 'loginUser'])->name('user.login');
Route::post('/register', [AuthController::class, 'register'])->name('user.register');
Route::post('/otp', [AuthController::class, 'otp'])->name('user.otp');
Route::post('/forgotPassword', [AuthController::class, 'forgotPassword'])->name('user.forgotPassword');


Route::group(['middleware' => ['auth:sanctum']], function () {
Route::post('/me', [\Modules\Auth\Http\Controllers\AuthController::class, 'me'])->name('user.me');
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

Route::post('/editProfile', [AuthController::class, 'editProfile'])->name('user.editProfile');
Route::post('/resetPassword', [AuthController::class, 'resetPassword'])->name('user.resetPassword');
Route::post('/login/as/user/{user}',[\Modules\Auth\Http\Controllers\API\AuthController::class, 'loginAsUser'])->middleware('permission:SuperAdmin');
});


