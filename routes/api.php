<?php

use App\Http\Resources\LogResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\PowerPlant\Entities\PowerPlant;
use Modules\User\Entities\User;
use Illuminate\Support\Facades\Route;
use Morilog\Jalali\Jalalian;
use Spatie\Activitylog\Models\Activity;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::get('hi', function () {
    return response()->json(['is_ok' => true]);
});

Route::group(['middleware' => ['auth:sanctum'],], function () {
    Route::get('/date', function () {
        $dateV = verta();
        $dateString = $dateV->format('Y l d F');

        return response()->json(['message' => $dateString]);
    });

    Route::get('/logs', function (Request $request) {
        //todo validation
        $query = Activity::query();

        if ($request->has('causer_id')) {
            $query->where('causer_id', $request->input('causer_id'));
        }
        if ($request->has('event')) {
            $query->where('event', $request->input('event'));
        }
        if ($request->has('log_name')) {
            $query->where('log_name', $request->input('log_name'));
        }

        $data = $query->orderBy('id', 'desc')->get();

        $data = $data->map(function ($item, $index) {
            $item->row_num = $index + 1; // Add index to each user
            $item->new_date = verta(Carbon::parse($item->created_at))->format('Y/m/d H:i:s');
            return $item;
        });
        return response()->json(['data' => LogResource::collection($data)]);
        //todo create separate controller for this functions
    }
    )->middleware('permission:log.list');

    Route::get('/filteringLogData', function (Request $request) {
        $events = Activity::distinct('event')->pluck('event');
        $logNames = Activity::distinct('log_name')->pluck('log_name');
        $causerIDs = Activity::distinct('causer_id')->pluck('causer_id');
        $causers = User::whereIn('id', $causerIDs)
            ->select('id', 'first_name', 'last_name')
            ->get();

        $logNameData = $logNames->map(function ($logName) {
            return ['label' => $logName, 'value' => $logName];
        })->toArray();

        $eventData = $events->map(function ($event) {
            return ['label' => $event, 'value' => $event];
        })->toArray();

        $causerData = $causers->map(function ($causer) {
            return [
                'label' => $causer->first_name . ' ' . $causer->last_name,
                'value' => $causer->id,
            ];
        })->toArray();

        return response()->json([
            'log_name' => $logNameData,
            'event' => $eventData,
            'causer_id' => $causerData,
        ]);
    }
    )->middleware('permission:log.list');;
});
