<?php

use App\Http\Controllers\LogController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//todo authentication needs to be added
Route::get('logs',[LogController::class,'index']);
Route::get('logs/{log}',[LogController::class,'show']);



Route::get('/', function () {
    return redirect()->route('login');
});






