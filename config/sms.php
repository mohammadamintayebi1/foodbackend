<?php

return [
    //sms patterns
    'patterns' => [
        'otp' => 'k6pg6k9rtyzbgq6',
        'forget_password' => '4y0zqewah3r0wxl',
        'save_resume' => '6ph8ybq4efz07c4',
        'accept_status_resume' => '142tbo0xcgalkox',
        'reject_status_resume' => 'pdwpnpqge34ctfp',
        'edit_status_resume' => '1cqshc4c7atfwve',
        'admin_add_user' => 'lpeqm4rn8vm6xv3',
        'online_event_reminder' => 'uf87zue0hge3ki7',
        'inPerson_event_reminder' => '598mvolwma028h7',
    ],


];
