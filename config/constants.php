<?php

return [
    'ADMIN_EMAIL' => 'admin@example.com',

    'STEAM_INTERNAL_LOSS' => 0.0185,
    'STEAM_A' => 1,
    'STEAM_B' => -0.01,
    'STEAM_C' => '5.7e-5',

    'GAS_INTERNAL_LOSS' => 0.0075,
    'GAS_A' => 0.99,
    'GAS_B' => -0.00075,
    'GAS_C' => '2e-5',

    'COMBINEDCYCLE_INTERNAL_LOSS' => 0.0735,
    'COMBINEDCYCLE_A' => 0.96,
    'COMBINEDCYCLE_B' => 0.0051,
    'COMBINEDCYCLE_C' => '9e-7',

    'TANKER_RATE' => 0.036,
    'RAILWAY_RATE' => 0.0051,
];
