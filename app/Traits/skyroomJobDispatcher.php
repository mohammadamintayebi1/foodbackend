<?php

namespace App\Traits;

use App\Jobs\createUniqueJoinEventLink;
use App\Jobs\inPersonEventsSms;


trait skyroomJobDispatcher
{

    public static function dispatchingJob($event, $user)
    {
        $startTime = $event->start_time;

        $timeDifference = strtotime($startTime) - time();

        $delayInHours = floor($timeDifference / 3600) - 3;

        $delayInHours = $delayInHours < 3 ? 0 : $delayInHours;

        createUniqueJoinEventLink::dispatch($event->id, $user->id)->onQueue('skyroomLinks')->delay(now()->addHours($delayInHours));
    }

    public static function inPersonDispatchingJob($event, $user)
    {
        $startTime = $event->start_time;

        $timeDifference = strtotime($startTime) - time();

        $delayInHours = floor($timeDifference / 3600) - 3;

        $delayInHours = $delayInHours < 3 ? 0 : $delayInHours;

        inPersonEventsSms::dispatch($event->id, $user->id)->onQueue('skyroomLinks')->delay(now()->addHours($delayInHours));

    }

}
