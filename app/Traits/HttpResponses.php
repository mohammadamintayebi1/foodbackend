<?php

namespace App\Traits;

trait HttpResponses
{


    public function basicResponse($message = null, $code = 200)
    {
        return response()->json(['message' => $message], $code);
    }
    public function errorResponse($message = null,$code = 400){
        return response()->json(['errors' => $message], $code);
    }
}
