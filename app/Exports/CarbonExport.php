<?php

namespace App\Exports;

use App\Models\CarbonFootPrint;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CarbonExport implements FromArray, WithHeadings
{
    protected $carbonRecords;

    public function __construct($carbonRecords)
    {
        $this->carbonRecords = $carbonRecords;
    }

    public function headings(): array
    {
        return [

            'وضعیت گزارشگیری',
            'شناسه نیروگاه',
            'نام نیروگاه',

            'مالکیت',
            'نوع نیروگاه',

            "حجم گاز طبیعی مصرف شده tons/day",
            "حجم گازوئيل مصرف شده tons/day",
            "حجم مازوت مصرف شده tons/day",

            'انتشار ناشی از احتراق tons/day',
            'انتشار ناشی از انتقال و فرآوری سوخت tons/day',

            "ردپای کربن ناشی از تولید برق g / kWh",
            "ردپای کربن در پایان شبکه انتقال g / kWh",
            'زمان',
        ];
    }

    public function array(): array
    {
        $data = [];

        foreach ($this->carbonRecords as $carbonRecord) {

            $data[] = [
                'وضعیت گزارشگیری' => $carbonRecord->is_ok ? 'موفق' : 'ناموفق',
                'شناسه نیروگاه' => $carbonRecord->powerPlant->id,
                'نام نیروگاه' => $carbonRecord->powerPlant->name,

                'مالکیت' => $carbonRecord->powerPlant->ownership == 'government' ? 'دولتی' : 'خصوصی',
                'نوع نیروگاه' => $carbonRecord->powerPlant->type,

                "حجم گاز طبیعی مصرف شده tons/day" => $carbonRecord->gas_v,
                "حجم گازوئيل مصرف شده tons/day" => $carbonRecord->gasoline_v,
                "حجم مازوت مصرف شده tons/day" => $carbonRecord->mazut_v,

                'انتشار ناشی از احتراق tons/day' => $carbonRecord->Ec,
                'انتشار ناشی از انتقال و فرآوری سوخت tons/day' => $carbonRecord->Et,

                "ردپای کربن ناشی از تولید برق g / kWh" => $carbonRecord->CF_p,
                "ردپای کربن در پایان شبکه انتقال g / kWh" => $carbonRecord->CF_t,
                'زمان' => verta(Carbon::parse($carbonRecord->created_at))->format('Y/m/d'),
            ];
        }

        return $data;
    }
}
