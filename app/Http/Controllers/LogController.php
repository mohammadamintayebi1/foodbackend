<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\User\Entities\User;
use Morilog\Jalali\Jalalian;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Role;

class LogController extends Controller
{
    protected $roleNames = [
        'SuperAdmin' => 'سوپر ادمین',
        'Univercity' => 'نماینده دانشگاه',
        'Customer' => 'کاربر سایت',
    ];

    protected function getModel($activity)
    {
        if ($activity->subject_type == null) {
            return null;
        }
        $modelNameSpace = $activity->subject_type;
        $modelClass = app($modelNameSpace);
        $model = $modelClass->withTrashed()->find($activity->subject_id);
        return $model ?? null;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->input('key') != 'rL05Q1vuw26I3988gZNyz2Q5v7JBNlcAPa8Ln0Tgu7SAIY2fEq') {
            abort(403);
        }

        $events = Activity::distinct('event')->pluck('event');
        $logNames = Activity::distinct('log_name')->pluck('log_name');
        $causerIDs = Activity::distinct('causer_id')->pluck('causer_id');

        $eventInput = $request->input('event') ?? 'all';
        $logNameInput = $request->input('log_name') ?? 'all';
        $causerIdInput = $request->input('causer_id') ?? 'all';
        $roleInput = $request->input('role') ?? 'all';


        $allRoles = Role::all();
        $foundRole = $allRoles->first(function ($singleRole) use ($roleInput) {
            return $singleRole->id == $roleInput;
        });

//        dd($foundRole);
//        dd($allRoles);
        $roles = [];
        foreach ($allRoles as $role) {
            $role->pName = $this->roleNames[$role->name];
        }
        $roles = $allRoles;
//                dd($allRoles);

        $causers = User::whereIn('id', $causerIDs)
            ->select('id', 'first_name', 'last_name')
            ->get();


        $activitiesQuery = Activity::query();

        $activitiesQuery = $request->input('event') != 'all' && $request->has('event') ? $activitiesQuery->where('event', '=', $request->input('event')) : $activitiesQuery;
        $activitiesQuery = $request->input('log_name') != 'all' && $request->has('log_name') ? $activitiesQuery->where('log_name', '=', $request->input('log_name')) : $activitiesQuery;
        $activitiesQuery = $request->input('causer_id') != 'all' && $request->has('causer_id') ? $activitiesQuery->where('causer_id', '=', $request->input('causer_id')) : $activitiesQuery;


        if ($roleInput != 'all') {
            $activitiesQuery->whereExists(function ($query) use ($foundRole) {
                $query->select(\DB::raw(1))
                    ->from('model_has_roles')
                    ->whereColumn('model_has_roles.model_id', 'activity_log.causer_id')
                    ->where('model_has_roles.model_type', '=', 'Modules\User\Entities\User') // Use your User model's actual class name
                    ->whereExists(function ($subQuery) use ($foundRole) {
                        $subQuery->select(\DB::raw(1))
                            ->from('roles')
                            ->whereColumn('roles.id', 'model_has_roles.role_id')
                            ->where('roles.name', '=', $foundRole->name);
                    });
            });
        }


        $activitiesQuery->orderBy('id', 'desc');
        $activities = $activitiesQuery->paginate(10);

        foreach ($activities as $activity) {
            $activity->roleName = $activity->causer ? $this->roleNames[$activity->causer->roles->first()->name] : null;
            $carbonActivity = Carbon::parse($activity->created_at);

            $carbonActivity->addHours(3)->addMinutes(30);

            $activity->created_at = Jalalian::fromCarbon($carbonActivity)->format('Y-m-d H:i:s');

            if (!$activity->subject) {
                $activity->subject = $this->getModel($activity);
            }
        }
//        dd($roles);
//        dd(($activities));

        return view('logs.index2',

            compact(['activities',
                'logNames', 'logNameInput',
                'causers', 'causerIdInput',
                'events', 'eventInput',
                'roles', 'roleInput'
            ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public
    function show(Request $request, $id)
    {

        if ($request->input('key') != 'rL05Q1vuw26I3988gZNyz2Q5v7JBNlcAPa8Ln0Tgu7SAIY2fEq') {
            abort(403);
        }

        $activity = Activity::find($id);

//        dd($activity->properties);
        $activity->created_at = Jalalian::fromDateTime($activity->created_at)->format('Y-m-d H:i:s');
        if (!$activity->subject) {
            $activity->subject = $this->getModel($activity);
        }
        return view('logs.show', compact('activity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        //
    }
}
