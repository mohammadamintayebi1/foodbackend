<?php

namespace App\Http\Resources\Event;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class myEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $isRegistered = $this->users->contains('id', auth()->id());
        $startDateTime = new Carbon($this->start_time);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'category' => $this->category->name,
            'event_type' => $this->event_type,
            'event_permission' => $this->event_permission,
            'is_held' => $this->is_held,
            'start_date' => $startDateTime->toDateString(), // Get only the date
            'start_hour' => $startDateTime->format('H:i'), // Get only the time
            'created_at' => $this->created_at,
            'image_url' => $this->getFirstMediaUrl('event_images') ?? null,
            'is_registered' => $isRegistered,
            'user_data' => [

            ]
        ];
    }
}
