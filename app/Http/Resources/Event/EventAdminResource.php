<?php

namespace App\Http\Resources\Event;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EventAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $startDateTime = new Carbon($this->start_time);

        $resourceArray = [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'category' => [
                'id' => $this->category->id,
                'label' => $this->category->name,
            ],
            'teacher' => $this->teacher
                ? [
                    'id' => $this->teacher->id,
                    'full_name' => $this->teacher->full_name,
                    'description' => $this->teacher->description,
                ]
                : null,
            'event_type' => $this->event_type,
            'event_permission' => $this->event_permission,
            'skyroom_room_id' => $this->skyroom_room_id,
            'is_held' => $this->is_held,
            'address' => $this->address,
            'start_date' => $startDateTime->toDateString(), // Get only the date
            'start_hour' => $startDateTime->format('H:i'), // Get only the time
            'created_at' => $this->created_at,
            'users' => $this->users,
            'image_url' => $this->getFirstMediaUrl('event_images') ?? null,
        ];

        // Conditionally add capacity if it exists
        if (!is_null($this->capacity)) {
            $resourceArray['capacity'] = $this->capacity;
        }

        // Conditionally add end time if it exists
        if (!is_null($this->registration_end_time)) {
            $resourceArray['registration_end_time'] = Carbon::parse($this->registration_end_time)->format('Y-m-d');
        }

        return $resourceArray;
    }
}
