<?php

namespace App\Http\Resources\Event;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user = auth()->user();
        $isRegistered = $this->users->contains('id', $user->id);
        $startDateTime = new Carbon($this->start_time);

        $resourceArray = [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'category' => $this->category->name,
            'event_type' => $this->event_type,
            'event_permission' => $this->event_permission,
            'is_held' => $this->is_held,
            'address' => $this->address,
            'start_date' => $startDateTime->toDateString(),
            'start_hour' => $startDateTime->format('H:i'),
            'created_at' => $this->created_at,
            'image_url' => $this->getFirstMediaUrl('event_images') ?? null,
            'is_registered' => $isRegistered,
            'teacher' => $this->teacher
                ? [
                    'id' => @$this->teacher->id,
                    'full_name' => @$this->teacher->full_name,
                    'description' => @$this->teacher->description,
                    'image' => @$this->teacher->getFirstMediaUrl('teacher_pics')
                ]
                : null,
        ];

        // Conditionally add capacity if it exists
        if (!is_null($this->capacity)) {
            $resourceArray['capacity'] = $this->capacity;
            // Calculate available capacity if capacity exists
            $availableCapacity = max(0, $this->capacity - $this->users->count());
            $resourceArray['available_capacity'] = $availableCapacity;
        }

        // Conditionally add registration end time if it exists
        if (!is_null($this->registration_end_time)) {
            $resourceArray['registration_end_time'] = Carbon::parse($this->registration_end_time)->format('Y-m-d');
        }

        if ($isRegistered) {
            // Include user-specific data when the user is registered
            $userEventData = $this->users->firstWhere('id', $user->id);
            $pivotData = $userEventData->pivot;

            $resourceArray['user_specific_data'] = [
                'user_type' => $pivotData->user_type ?? null,
                'user_status' => $pivotData->user_status ?? null,
                'skyroom_link' => $pivotData->skyroom_link ?? null,
            ];
        }

        return $resourceArray;
    }
}
