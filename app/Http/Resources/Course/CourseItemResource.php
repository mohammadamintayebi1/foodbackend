<?php

namespace App\Http\Resources\Course;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'course_id' => $this->course_id,
            'order_id' => $this->order_id,
            'title' => $this->title,
            'description' => $this->description,

            'video_file' => $this->hasMedia('course_item_video') && ($this->isRegistered || $this->order_id == 1) ?
                $this->getFirstMediaUrl('course_item_video') : null,

            'video_file_name' => $this->hasMedia('course_item_video') ?
                $this->getFirstMedia('course_item_video')->file_name : null,

            'teacher_file' => $this->hasMedia('course_item_teacher_file') && ($this->isRegistered || $this->order_id == 1) ?
                $this->getFirstMediaUrl('course_item_teacher_file') : null,

            'teacher_file_name' => $this->hasMedia('course_item_teacher_file') ?
                $this->getFirstMedia('course_item_teacher_file')->file_name : null,

        ];
    }
}
