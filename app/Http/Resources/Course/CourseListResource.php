<?php

namespace App\Http\Resources\Course;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Course\Transformers\CourceCategoryResource;

class CourseListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $courseItems = $this->courseItems;

        $courseItems->each(function ($courseItem) {
            $courseItem->isRegistered = $this->isRegistered;
        });

        return [
            'id' => $this->id,
            'course_image' => $this->getFirstMediaUrl('course_main_images') ?? null,
            'title' => $this->title,
            'description' => $this->description,
            'is_active' => $this->is_active,
            'course_length' => $this->course_length,
            'category_name' => @$this->course_category->title,
            'category' => new CourceCategoryResource($this->course_category),
            'teacher_name' => @$this->teacher->full_name,
            'course_items_count' => $this->courseItems()->count(),
            'course_items' => @CourseItemResource::collection(@$courseItems)
        ];
    }
}
