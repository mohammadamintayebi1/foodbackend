<?php

namespace App\Http\Resources\Course;

use App\Http\Resources\Event\TeacherResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Course\Transformers\CourceCategoryResource;

class CourseShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $courseItems = $this->courseItems;

        $courseItems->each(function ($courseItem) {
            $courseItem->isRegistered = $this->isRegistered;
        });

        $courseEvents = $this->events()->get();
        $eventData = [];
        foreach ($courseEvents as $courseEvent) {
            $eventData [] = [
                'value' => $courseEvent->id,
                'label' => $courseEvent->name,
            ];
        }

        return [
            'id' => $this->id,
            'course_image' => $this->getFirstMediaUrl('course_main_images') ?? null,
            'title' => $this->title,
            'description' => $this->description,
            'is_active' => $this->is_active,
            'course_length' => $this->course_length,
            'category_name' => @$this->course_category->title,

            'student_count' => $this->students()->count(),

//            'is_registered' => @$this->additional['is_registered'],

            'is_registered' => @$this->isRegistered,
            'is_finished' => $this->isFinished,
            'category' => new CourceCategoryResource($this->course_category),
            'teacher' => $this->teacher ? new TeacherResource($this->teacher) : null,
            'course_items' => CourseItemResource::collection($courseItems),
            'events' => $eventData,

        ];
    }
}
