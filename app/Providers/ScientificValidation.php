<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ScientificValidation extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        Validator::extend('scientific', function($attribute, $value, $parameters, $validator) {
            return preg_match('/^-?\d+(\.\d+)?e[+\-]?\d+$/i', $value);
        });

        Validator::replacer('scientific', function ($message, $attribute, $rule, $parameters) {
            // return str_replace(':attribute', $attribute, 'The :attribute must be a valid scientific number format.');
            return str_replace(':attribute', $attribute, 'ورودی :attribute باید به صورت نماد علمی باشد.');
        });
    }
}
