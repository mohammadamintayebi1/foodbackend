<?php

namespace App\Helpers;

use App\Http\Controllers\Controller;
use DateTimeZone;
use Hekmatinasser\Verta\Verta;

class Vira extends Controller
{

    static $messages = [
        'required' => 'فیلد :attribute اجباری است',
        'size' => 'تعداد کارکترهای وارد شده فیلد :attribute صحیح نمی‌باشد',
        'unique' => ':attribute وارد شده تکراری می‌باشد',
        'exists' => ':attribute وارد شده در سامانه موجود نمی‌باشد',
    ];

    static $attribute = [
        'first_name' => 'نام',
        'last_name' => 'نام خانوادگی',
        'phone_number' => 'موبایل',
        'title' => 'عنوان',
        'text' => 'متن',
    ];

    public static function send_sms($input_data, $pattern_code, $phone_number)
    {
        $client = new \SoapClient("https://ippanel.com/class/sms/wsdlservice/server.php?wsdl");
        $username = env('FARAZ_SMS_USERNAME');
        $pass = env('FARAZ_SMS_PASSWORD');
        $fromNum = "+983000505";
        $toNum = array($phone_number);
        $r = $client->sendPatternSms($fromNum, $toNum, $username, $pass, $pattern_code, $input_data);
    }

    public static function fa2en($data, $number_format = null)
    {
        $num_fa = ['۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '۰'];
        $num_en = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];

        $data = str_replace($num_fa, $num_en, $data);
        if ($number_format == 1) {
            $data = number_format($data);
        } elseif ($number_format == 2) {
            $data = str_replace(',', '', $data);
        }
        return $data;
    }

    public static function en2fa($data, $number_format = null)
    {
        $num_en = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
        $num_fa = ['۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '۰'];
        if ($number_format == 1) {
            $data = number_format($data);
        } elseif ($number_format == 2) {
            $data = str_replace(',', '', $data);
        }
        $data = str_replace($num_en, $num_fa, $data);
        return $data;
    }

    public static function jalal2shamsi($time, $type = null, $sep = '-')
    {
        if ($time) {
            // if (preg_match('/\d{4}' . $sep . '\d{2}' . $sep . '\d{2}/', $time)) {
            $dt = new \DateTime($time);
            $dt->setTimezone(new DateTimeZone('Asia/Tehran'));
            $date = explode($sep, $dt->format('Y-m-d'));
            $time = $dt->format('H:i:s');
            $date = Verta::getJalali($date[0], $date[1], $date[2]);
            if ($type == null) {
                return "$date[0]-$date[1]-$date[2] $time";
            } elseif ($type == 'time') {
                return $dt->format('H:i');
            } else {
                return "$date[0]-$date[1]-$date[2]";
            }
            // } else {
            //     return null;
            // }
        }
    }

    /**
     * @throws \Exception
     */
    public static function shamsi2jalal($time, $type = null, $sep = '/'): string
    {
        if ($time) {
            // if (preg_match('/\d{4}' . $sep . '\d{2}' . $sep . '\d{2}/', $time)) {
            $dt = new \DateTime($time);
            $dt->setTimezone(new DateTimeZone('Asia/Tehran'));
            if ($type == null) {
                $date = explode($sep, $dt->format('Y-m-d'));
                $time = $dt->format('H:i:s');
                $date = Verta::getGregorian($date[0], $date[1], $date[2]);
                return "$date[0]-$date[1]-$date[2] $time";
            } else {
                $date = explode($sep, $dt->format('Y-m-d'));
                $date = Verta::getGregorian($date[0], $date[1], $date[2]);
                return "$date[0]-$date[1]-$date[2]";
            }
            // } else {
            //     return null;
            // }
        }
    }

    public static function pushe($message, $admins)
    {

        $ch = curl_init('https://api.pushe.co/v2/messaging/notifications/');

        curl_setopt_array($ch, array(
            CURLOPT_POST => 1,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Token " . env('PUSHE_TOKEN'),
            ),
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
            'app_ids' => env('PUSHE_APP_ID'),
            'data' => $message,
            'filters' => array(
                'device_id' => $admins,
            ),
        )));

        curl_exec($ch);
        curl_close($ch);
    }

    public static function findArray($array, $key_name, $val, $value)
    {
        foreach ($array as $key) {
            $key = is_array($key) ? (object)$key : $key;
            if ($key->$key_name == $val) {
                return $key->$value;
            }
        }
    }
}
