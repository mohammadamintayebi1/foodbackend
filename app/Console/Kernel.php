<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Stringable;
use Modules\CarbonFootprint\Entities\CarbonFootprint;
use Modules\CarbonFootprint\Http\Controllers\CarbonFootprintController;
use Modules\PowerPlant\Entities\PowerPlant;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            Log::info('Schedule running...');
            $controller = (new CarbonFootprintController());
            $date = Carbon::now()->subDays(1);
            $controller->calculateCarbonFootprintData(null, null, $date,);
        })->dailyAt('13:30')->timezone('UTC');

//        $schedule->command('telescope:prune')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
