<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class MigrateBothDatabases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:both';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate fresh for both databases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "Migrating Table Started \n";
        Schema::connection('mysql2')->dropIfExists('weather_data');
        Artisan::call('migrate:fresh', ['--seed' => true]);

        echo "\n" . "Migration Completed Successfully!";
//        echo Artisan::output();

        return 1;
    }
}
