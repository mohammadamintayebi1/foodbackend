<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // ایجاد دسترسی ها
        Permission::insert([
            ['name' => 'SuperAdmin', 'guard_name' => 'web'],

            ['name' => 'role.list', 'guard_name' => 'web'],

            ['name' => 'log.list', 'guard_name' => 'web'],

            ['name' => 'user.list', 'guard_name' => 'web'],
            ['name' => 'user.store', 'guard_name' => 'web'],
            ['name' => 'user.edit', 'guard_name' => 'web'],
            ['name' => 'user.delete', 'guard_name' => 'web'],
            ['name' => 'user.search', 'guard_name' => 'web'],

            ['name' => 'ticket.delete', 'guard_name' => 'web'],
            ['name' => 'ticket.list', 'guard_name' => 'web'],
            ['name' => 'ticket.search', 'guard_name' => 'web'],
            ['name' => 'ticket.all', 'guard_name' => 'web'],
            ['name' => 'ticket.store', 'guard_name' => 'web'],

            ['name' => 'powerPlant.list', 'guard_name' => 'web'],
            ['name' => 'powerPlant.store', 'guard_name' => 'web'],
            ['name' => 'powerPlant.edit', 'guard_name' => 'web'],
            ['name' => 'powerPlant.delete', 'guard_name' => 'web'],

            ['name' => 'fuelTransportation.list', 'guard_name' => 'web'],
            ['name' => 'fuelTransportation.store', 'guard_name' => 'web'],
            ['name' => 'fuelTransportation.edit', 'guard_name' => 'web'],
            ['name' => 'fuelTransportation.delete', 'guard_name' => 'web'],

            ['name' => 'fuel.list', 'guard_name' => 'web'],
            ['name' => 'fuel.store', 'guard_name' => 'web'],
            ['name' => 'fuel.edit', 'guard_name' => 'web'],
            ['name' => 'fuel.delete', 'guard_name' => 'web'],

        ]);

        // ایجاد رول ها

        $SuperAdmin = Role::create([
            'name' => "SuperAdmin",
            'guard_name' => 'web',
        ]);
        $admin = Role::create([
            'name' => "admin",
            'guard_name' => 'web',
        ]);
        $reporter = Role::create([
            'name' => "reporter",
            'guard_name' => 'web',
        ]);

        // اختصاص دسترسی ها به رول ها
        $SuperAdmin->syncPermissions([
            'SuperAdmin',

            'role.list',

            'log.list',

            'user.list',
            'user.store',
            'user.edit',
            'user.delete',

            'powerPlant.list',
            'powerPlant.store',
            'powerPlant.edit',
            'powerPlant.delete',

            'fuelTransportation.list',
            'fuelTransportation.store',
            'fuelTransportation.edit',

            'fuel.list',
            'fuel.store',
            'fuel.edit',
            'fuel.delete',

            'ticket.delete',
            'ticket.list',
            'ticket.search',
            'ticket.all',
        ]);

        $admin->syncPermissions([

            'powerPlant.list',
            'fuelTransportation.list',
            'fuel.list',

            'ticket.store',
            'ticket.list',

            'user.list',
        ]);

        $reporter->syncPermissions([
            'powerPlant.list',
            'fuelTransportation.list',
            'ticket.store',
            'ticket.list',

        ]);

        // ایجاد کاربران تست
        $user_admin = User::create([
            "first_name" => "ادمین",
            "last_name" => "کل",
            "phone_number" => "09121111111",
            "password" => Hash::make("12345"),
            "other" => [
                "status" => 1
            ],
        ]);

        $user_manager = User::create([
            "first_name" => "ادمین",
            "last_name" => "عادی",
            "phone_number" => "09122222222",
            "password" => Hash::make("12345"),
            "other" => [
                "status" => 1
            ],
        ]);


        $user_admin->assignRole($SuperAdmin);
        $user_manager->assignRole($admin);
    }
}
