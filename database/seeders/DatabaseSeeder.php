<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Guest\Entities\Guest;
use Modules\PowerPlant\Database\Seeders\LocationSeeder;
use Modules\PowerPlant\Database\Seeders\PowerPlantSeeder;
use Modules\User\Entities\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);

        for ($i = 1; $i <= 50; $i++) {
            Guest::create([
                "code" => "N_$i",
                "full_name" => "کاربر تستی " . $i,
            ]);
        }
    }
}
