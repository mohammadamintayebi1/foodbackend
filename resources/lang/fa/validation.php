<?php

return [
    'accepted' => 'فیلد :attribute باید پذیرفته شود.',
    'accepted_if' => 'فیلد :attribute باید پذیرفته شود زمانی که :other برابر :value است.',
    'active_url' => 'فیلد :attribute یک URL معتبر نیست.',
    'after' => 'فیلد :attribute باید یک تاریخ پس از :date باشد.',
    'after_or_equal' => 'فیلد :attribute باید یک تاریخ پس از یا مساوی با :date باشد.',
    'alpha' => 'فیلد :attribute فقط می‌تواند شامل حروف باشد.',
    'alpha_dash' => 'فیلد :attribute فقط می‌تواند شامل حروف، اعداد، خط تیره و آندرلاین باشد.',
    'alpha_num' => 'فیلد :attribute فقط می‌تواند شامل حروف و اعداد باشد.',
    'array' => 'فیلد :attribute باید یک آرایه باشد.',
    'before' => 'فیلد :attribute باید یک تاریخ قبل از :date باشد.',
    'before_or_equal' => 'فیلد :attribute باید یک تاریخ قبل از یا مساوی با :date باشد.',
    'between' => [
        'numeric' => 'فیلد :attribute باید بین :min و :max باشد.',
        'file' => 'فیلد :attribute باید بین :min و :max کیلوبایت باشد.',
        'string' => 'فیلد :attribute باید بین :min و :max کاراکتر باشد.',
        'array' => 'فیلد :attribute باید بین :min و :max آیتم داشته باشد.',
    ],
    'boolean' => 'فیلد :attribute باید true یا false باشد.',
    'confirmed' => 'تطابق :attribute صحیح نیست.',
    'current_password' => 'رمز عبور نادرست است.',
    'date' => 'فیلد :attribute یک تاریخ معتبر نیست.',
    'date_equals' => 'فیلد :attribute باید یک تاریخ مساوی با :date باشد.',
    'date_format' => 'فیلد :attribute با فرمت :format مطابقت ندارد.',
    'declined' => 'فیلد :attribute باید رد شود.',
    'declined_if' => 'فیلد :attribute باید رد شود زمانی که :other برابر :value است.',
    'different' => 'فیلد :attribute و :other باید متفاوت باشند.',
    'digits' => 'فیلد :attribute باید :digits رقم باشد.',
    'digits_between' => 'فیلد :attribute باید بین :min و :max رقم باشد.',
    'dimensions' => 'فیلد :attribute دارای ابعاد تصویر نامعتبر است.',
    'distinct' => 'فیلد :attribute دارای مقدار تکراری است.',
    'email' => 'فیلد :attribute باید یک آدرس ایمیل معتبر باشد.',
    'ends_with' => 'فیلد :attribute باید با یکی از این مقادیر پایان یابد: :values.',
    'enum' => 'مقدار انتخاب شده برای :attribute نامعتبر است.',
    'exists' => 'مقدار انتخاب شده برای :attribute نامعتبر است.',
    'file' => 'فیلد :attribute باید یک فایل باشد.',
    'filled' => 'فیلد :attribute باید مقدار داشته باشد.',
    'gt' => [
        'numeric' => 'فیلد :attribute باید بزرگتر از :value باشد.',
        'file' => 'فیلد :attribute باید بزرگتر از :value کیلوبایت باشد.',
        'string' => 'فیلد :attribute باید بزرگتر از :value کاراکتر باشد.',
        'array' => 'فیلد :attribute باید بیشتر از :value آیتم داشته باشد.',
    ],
    'gte' => [
        'numeric' => 'فیلد :attribute باید بزرگتر یا مساوی با :value باشد.',
        'file' => 'فیلد :attribute باید بزرگتر یا مساوی با :value کیلوبایت باشد.',
        'string' => 'فیلد :attribute باید بزرگتر یا مساوی با :value کاراکتر باشد.',
        'array' => 'فیلد :attribute باید شامل :value آیتم یا بیشتر باشد.',
    ],
    'image' => 'فیلد :attribute باید یک تصویر باشد.',
    'in' => 'مقدار انتخاب شده برای :attribute نامعتبر است.',
    'in_array' => 'فیلد :attribute در :other وجود ندارد.',
    'integer' => 'فیلد :attribute باید یک عدد صحیح باشد.',
    'ip' => 'فیلد :attribute باید یک آدرس IP معتبر باشد.',
    'ipv4' => 'فیلد :attribute باید یک آدرس IPv4 معتبر باشد.',
    'ipv6' => 'فیلد :attribute باید یک آدرس IPv6 معتبر باشد.',
    'json' => 'فیلد :attribute باید یک رشته JSON معتبر باشد.',
    'lt' => [
        'numeric' => 'فیلد :attribute باید کمتر از :value باشد.',
        'file' => 'فیلد :attribute باید کمتر از :value کیلوبایت باشد.',
        'string' => 'فیلد :attribute باید کمتر از :value کاراکتر باشد.',
        'array' => 'فیلد :attribute باید کمتر از :value آیتم داشته باشد.',
    ],
    'lte' => [
        'numeric' => 'فیلد :attribute باید کمتر یا مساوی با :value باشد.',
        'file' => 'فیلد :attribute باید کمتر یا مساوی با :value کیلوبایت باشد.',
        'string' => 'فیلد :attribute باید کمتر یا مساوی با :value کاراکتر باشد.',
        'array' => 'فیلد :attribute نباید بیشتر از :value آیتم داشته باشد.',
    ],
    'mac_address' => 'فیلد :attribute باید یک آدرس MAC معتبر باشد.',
    'max' => [
        'numeric' => 'فیلد :attribute نباید بیشتر از :max باشد.',
        'file' => 'فیلد :attribute نباید بیشتر از :max کیلوبایت باشد.',
        'string' => 'فیلد :attribute نباید بیشتر از :max کاراکتر باشد.',
        'array' => 'فیلد :attribute نباید بیشتر از :max آیتم داشته باشد.',
    ],
    'mimes' => 'فیلد :attribute باید یک فایل از نوع :values باشد.',
    'mimetypes' => 'فیلد :attribute باید یک فایل از نوع :values باشد.',
    'min' => [
        'numeric' => 'فیلد :attribute باید حداقل :min باشد.',
        'file' => 'فیلد :attribute باید حداقل :min کیلوبایت باشد.',
        'string' => 'فیلد :attribute باید حداقل :min کاراکتر باشد.',
        'array' => 'فیلد :attribute باید حداقل :min آیتم داشته باشد.',
    ],
    'multiple_of' => 'فیلد :attribute باید یک چندتایی از :value باشد.',
    'not_in' => 'مقدار انتخاب شده برای :attribute نامعتبر است.',
    'not_regex' => 'فرمت :attribute نامعتبر است.',
    'numeric' => 'فیلد :attribute باید یک عدد باشد.',
    'password' => 'رمز عبور اشتباه است.',
    'present' => 'فیلد :attribute باید وجود داشته باشد.',
    'prohibited' => 'فیلد :attribute ممنوع است.',
    'prohibited_if' => 'فیلد :attribute ممنوع است زمانی که :other برابر :value است.',
    'prohibited_unless' => 'فیلد :attribute ممنوع است مگر اینکه :other در :values باشد.',
    'prohibits' => 'فیلد :attribute از وجود :other جلوگیری می‌کند.',
    'regex' => 'فرمت :attribute نامعتبر است.',
    'required' => 'فیلد :attribute الزامی است.',
    'required_array_keys' => 'فیلد :attribute باید شامل مقادیر :values باشد.',
    'required_if' => 'فیلد :attribute الزامی است زمانی که :other برابر :value است.',
    'required_unless' => 'فیلد :attribute الزامی است مگر اینکه :other در :values باشد.',
    'required_with' => 'فیلد :attribute الزامی است زمانی که :values وجود دارد.',
    'required_with_all' => 'فیلد :attribute الزامی است زمانی که :values وجود دارد.',
    'required_without' => 'فیلد :attribute الزامی است زمانی که :values وجود ندارد.',
    'required_without_all' => 'فیلد :attribute الزامی است زمانی که هیچکدام از :values وجود ندارند.',
    'same' => 'فیلد :attribute و :other باید یکسان باشند.',
    'size' => [
        'numeric' => 'فیلد :attribute باید برابر با :size باشد.',
        'file' => 'فیلد :attribute باید برابر با :size کیلوبایت باشد.',
        'string' => 'فیلد :attribute باید برابر با :size کاراکتر باشد.',
        'array' => 'فیلد :attribute باید شامل :size آیتم باشد.',
    ],
    'starts_with' => 'فیلد باید با یکی از این مقادیر آغاز شود: :values.',
    'string' => 'فیلد باید یک رشته باشد.',
    'timezone' => 'فیلد باید یک منطقه زمانی معتبر باشد.',
    'unique' => ' مقدار :attribute قبلاً استفاده شده است.',
    'uploaded' => 'آپلود فایل با خطا مواجه شد.',
    'url' => 'فیلد باید یک آدرس اینترنتی معتبر باشد.',
    'uuid' => 'فیلد باید یک UUID معتبر باشد.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
|--------------------------------------------------------------------------
| Custom Validation Attributes
|--------------------------------------------------------------------------
|
| The following language lines are used to swap our attribute placeholder
| with something more reader friendly such as "E-Mail Address" instead
| of "email". This simply helps us make our message more expressive.
|
*/

    'attributes' => [
        'phone_number' => 'شماره تلفن همراه',
        'password' => 'رمز عبور',
        'new_pass' => 'رمز عبور جدید',
        'first_name' => 'نام',
        'last_name' => 'نام خانوادگی',
        'video_file' => 'ویدئو',

        'gas_fuel_transportation' => 'خط انتقال گاز طبیعی', //todo validation for inside object
        'gas_fuel_transportation.transportationsLength' => 'طول خط لوله گاز طبیعی',
        'gas_fuel_transportation.number' => 'تعداد ایستگاه های تقویت فشار گاز طبیعی',

        'mazut_fuel_transportation' => 'خط انتقال مازوت',
        'mazut_fuel_transportation.type' => 'نوع انتقال مازوت',
        'mazut_fuel_transportation.density' => 'چگالی سوخت مصرفی وسیله نقلیه حامل مازوت',
        'mazut_fuel_transportation.transportationsLength' => 'طول مسیر انتقال مازوت',
        'mazut_fuel_transportation.number' => 'نرخ مصرف سوخت وسیله نقلیه حامل مازوت',

        'gasoline_fuel_transportation' => 'خط انتقال گازوئیل',
        'gasoline_fuel_transportation.type' => 'نوع انتقال گازوئیل',
        'gasoline_fuel_transportation.density' => 'چگالی سوخت مصرفی وسیله نقلیه حامل گازوئیل',
        'gasoline_fuel_transportation.transportationsLength' => 'طول مسیر انتقال گازوئیل',
        'gasoline_fuel_transportation.number' => 'نرخ مصرف سوخت وسیله نقلیه حامل گازوئیل',

        'fuel_name' => 'نام سوخت',
        'fuel_type' => 'نوع سوخت',
        'fuel_density' => 'چگالی سوخت',
        'fuel_lower_heating_value' => 'ارزش حرارتی پایین سوخت مصرفی',
        'fuel_transportation_release' => 'ضریب انتشار انتقال سوخت',
        'fuel_processing_release' => 'ضریب انتشار فراوری سوخت',
        'fuel_consumption_release' => 'ضریب انتشار احتراق سوخت',

        'gas_fuel' => 'نوع گاز مصرفی نیروگاه',
        'mazut_fuel' => 'نوع مازوت مصرفی نیروگاه',
        'gasoline_fuel' => 'نوع گازوئیل مصرفی نیروگاه',


        'code' => 'کد نیروگاه',
        'type' => 'نوع نیروگاه',
        'location' => 'استان',
        'ownership' => 'مالکیت نیروگاه',

    ],
];
