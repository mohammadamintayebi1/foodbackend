@include('layouts.header')
<div class="container-fluid">
    <!-- Begin Page Header-->

    <!-- End Page Header -->
    <!-- Begin Row -->
    <div class="row flex-row">
        <!-- Begin Facebook -->
        <div class="col-xl-4 col-md-6 col-sm-6">
            <div class="widget widget-12 has-shadow">
                <div class="widget-body">
                    <div class="media">
                        <div class="align-self-center ml-5 mr-5">
                            <i class="ion-person-stalker text-twitter"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="title text-twitter">تعداد اعضا</div>
                            <div class="number">۱،۴۵۰</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Facebook -->
        <!-- Begin Twitter -->
        <div class="col-xl-4 col-md-6 col-sm-6">
            <div class="widget widget-12 has-shadow">
                <div class="widget-body">
                    <div class="media">
                        <div class="align-self-center ml-5 mr-5">
                            <i class="ion-social-buffer-outline text-twitter"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="title text-twitter">رزومه ها</div>
                            <div class="number">۱،۱۰۰</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Twitter -->
        <!-- Begin Linkedin -->
        <div class="col-xl-4 col-md-6 col-sm-6">
            <div class="widget widget-12 has-shadow">
                <div class="widget-body">
                    <div class="media">
                        <div class="align-self-center ml-5 mr-5">
                            <i class="ion-compose text-twitter"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="title text-twitter">وضعیت رزومه</div>
                            <div class="number">
                                @switch($status)
                                    @case(1)
                                        در انتظار تایید
                                    @break

                                    @case(2)
                                        نیازمند اصلاح
                                    @break

                                    @case(3)
                                        تایید شده
                                    @break

                                    @default
                                        ارسال نشده
                                @endswitch
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Linkedin -->
    </div>
    <!-- End Row -->
    <!-- Begin Row -->
    <div class="row flex-row">
        <div class="col-xl-12 col-md-6">
            <!-- Begin Widget 09 -->
            <div class="widget widget-09 has-shadow">
                <!-- Begin Widget Header --
                <div class="widget-header d-flex align-items-center">
                    <h2>خوش آمدید</h2>
                    <div class="widget-options">
                        <button type="button" class="btn btn-shadow">
                            تکمیل پروفایل
                        </button>
                    </div>
                </div>
                <!-- End Widget Header -->
                <!-- Begin Widget Body -->
                <div class="widget-body">
                    <div class="row">
                        <div class="col-xl-12 col-12 no-padding">
                            <div class="p-4 text-justify" style="color: #222">
                                <p style="font-size: 1.3rem; line-height: 3rem;">
                                    دستیابی به توسعه اقتصادی پایدار، یکی از مهم‌ترین دغدغه کشورهای مختلف بوده و یکی از مهمترین عوامل آن، توجه ویژه به موضوع کارآفرینی است. توسعه کارآفرینی دانش‌پایه، نیازمند توانمندسازی افراد در حوزه مهارت‌های نرم و تخصصی کارآفرینی می‌باشد که طرح ملی ترویج کارآفرینی، با هدف آموزش و توانمندسازی عادلانه دانشجویان و دانش‌ آموختگان دانشگاه‌های کشور برای اولین بار توسط معاونت نوآوری وزارت علوم، تحقیقات و فناوری و با همکاری پارک علم و فناوری دانشگاه صنعتی امیرکبیر و بر بستر شبکه مهارت و اشتغال راهه‌کار برگزار می‌گردد.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Widget 09 -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- End Container -->
@include('layouts.footer')
