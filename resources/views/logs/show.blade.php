<x-admin-layout>

    <h1 class="text-lg font-bold mt-40 mb-2 text-center px-4 py-2 rounded bg-blue-400 w-50 mx-auto">Log Data</h1>
    <div class="text-center mx-auto  block w-50  border border-2 border-fuchsia-400 rounded py-2 bg-white">

        <p class="mt-1">Log ID : {{$activity->id}}</p>
        <p class="mt-1">Log Category : {{$activity->log_name}}</p>
        <p class="mt-1 "> Event : <span class="font-bold">{{ucfirst($activity->event)}}</span></p>

        <p class="mt-1"> Caused
            By
            : {{$activity->causer ? $activity->causer->first_name.' '.$activity->causer->last_name. " (id :   ".$activity->causer->id.")" : "Unknown"}}
        {{--                : {{$activity->causer ? $activity->causer->first_name.' '.$activity->causer->last_name :  'null'}}</p>--}}


        <p class="mt-1">Performed on
            : {{$activity->subject ? class_basename($activity->subject).' Model with ID '. $activity->subject->id  : "Unknown"}}</p>
        {{--        <p class="mt-1">Subject ID : {{$activity->subject->id ?? "null"}}</p>--}}

        <p class="mt-1">Date and Time : {{$activity->created_at}}</p>
    </div>
    @if($activity->event == 'created')
        <div>
            <div class="border border-2 rounded pt-3 pb-5 px-10 w-50 mx-auto mt-20 bg-white">
                <p class="text-center mb-10 bg-green-400 px-2 py-1 rounded">Created Data</p>
                @foreach($activity->properties['attributes'] as $key => $value)
                    @if($key != 'password')
                        <p class="text-center mt-1"> {{$key}} => {{$value}}</p>
                    @endif

                @endforeach
            </div>
        </div>
    @elseif($activity->event == 'updated')
        <div class="flex flex-row justify-around mt-20 w-50 mx-auto ">

            <div class="border border-2 rounded pt-3 pb-5 px-10 bg-white">
                <p class="text-center mb-10 bg-red-400 px-2 py-1 rounded ">Old Data</p>
                @foreach($activity->properties['old'] as $key => $value)
                    <p> {{$key}} => {{$value}}</p>
                @endforeach
            </div>
            <div class="border border-2 rounded pt-3 pb-5 px-10 bg-white">
                <p class="text-center mb-10 bg-green-400 px-2 py-1 rounded">Updated Data</p>
                @foreach($activity->properties['attributes'] as $key => $value)
                    <p> {{$key}} => {{$value}}</p>
                @endforeach
            </div>
        </div>
    @elseif($activity->event == 'deleted')
        <div class="border border-2 rounded pt-3 pb-5 px-10 w-50 mx-auto mt-20 bg-white">
            <p class="text-center mb-10 bg-red-400 px-2 py-1 rounded">Old Data</p>
            @foreach($activity->properties['old'] as $key => $value)
                @if($key != 'password')
                    <p class="text-center mt-1"> {{$key}} => {{$value}}</p>
                @endif
            @endforeach
        </div>
    @else

        @if($activity->properties != [] && !empty($activity->properties))
            <div>

                <div class="border border-2 rounded pt-3 pb-5 px-10 w-50 mx-auto mt-20 bg-white">
                    <p class="text-center mb-10 bg-green-400 px-2 py-1 rounded"> Data</p>
                    @foreach($activity->properties as $key => $value)
                        @if($key != 'password')
                            <p class="text-center mt-1"> {{ucfirst($key)}} => {{$value}}</p>
                        @endif

                    @endforeach
                </div>
            </div>
        @endif
    @endif


    <div class="text-center mx-auto mt-20 "><a class=" bg-orange-500 rounded px-4 py-2 "
                                               href="/logs?key=rL05Q1vuw26I3988gZNyz2Q5v7JBNlcAPa8Ln0Tgu7SAIY2fEq"
        >Logs index</a></div>
</x-admin-layout>
