@php use Illuminate\Support\Facades\Auth; @endphp


    <!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"
            defer>
    </script>

    <script src="https://cdn.tailwindcss.com"></script>


    {{-- <script
        type="text/javascript"
        src={{ asset('/assets/js/admin.js') }}
        defer
         ></script> --}}
    <meta name="_token" content="{{ csrf_token() }}">
    {{--    <script src="{{ asset('js/custom.js') }}" defer></script>--}}

    {{--    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>--}}

    <style>
        .back-tester {
            background-color: #d2d2d2;
        }
    </style>
    {{--    @vite(['/resources/js/app.js']) --}}


    <!-- Include Select2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" rel="stylesheet"/>

    <!-- Include jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer></script>

    <!-- Include Select2 JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js" defer></script>


</head>

<body class="back-tester">
<main>
    <div>
        @if (session()->has('success'))
            <div class="alert alert-success mt-5 text-center">
                {{ session()->get('success') }}
                {{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button> --}}
            </div>
        @endif
        @if (session()->has('warning'))
            <div class="alert alert-warning  mt-5 text-center">
                {{ session()->get('warning') }}
                {{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button> --}}
            </div>
        @endif
        @if (session()->has('danger'))
            <div class="alert alert-danger  mt-5 text-center">
                {{ session()->get('danger') }}
                {{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button> --}}
            </div>
        @endif
    </div>


    <h1 class=" mt-20 text-center text-3xl font-bold underline">
        Log
    </h1>
    <form
        class="flex flex-row justify-between w-25 mx-auto mt-20 items-center"
        action="/logs" method="get">

        <select id="searchableSelect" class="form-control">
            <option>Text</option>
            @foreach ($options as $option)
                <option value="{{ $option->id }}">{{ $option->name }}</option>
            @endforeach
        </select>

        <label>Type:
            <select name="event">
                <option value="all">All</option>
                @foreach($events as $event)
                    <option value="{{$event}}">{{ucfirst($event)}}</option>
                @endforeach
            </select>
        </label>
        <label>Type:
            <select name="log_name">
                <option value="all">All</option>
                @foreach($logNames as $logName)
                    <option value="{{$logName}}">{{ucfirst($logName)}}</option>
                @endforeach
            </select>
        </label>

        <button type="submit" class="bg-green-300 py-1 px-2 rounded">Search</button>
        <a href="/logs"
           class="text-center bg-blue-300 px-2 py-1 rounded"
        >Clear All</a>
    </form>



    <div class="text-center mt-20">
        <div class="px-5 mx-5">

            <table
                class="table text-center align-middle">
                <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Name</th>
                    <th scope="col">user</th>
                    <th scope="col">user id</th>
                    <th scope="col">subject</th>
                    <th scope="col">subject id</th>
                    <th scope="col">event</th>
                    <th scope="col" class=''>Data & Time</th>
                    <th scope="col" class="w-50">Data</th>
                </tr>
                </thead>
                <tbody>

                @foreach($activities as $activity)
                    <tr class="my-5">
                        <th scope="row">{{$activity->id}}</th>
                        <th scope="row">{{$activity->log_name}}</th>

                        <td class="">
                            {{$activity->causer ? $activity->causer->first_name.' '.$activity->causer->last_name :  'null'}}
                        </td>
                        <td class="">
                            {{$activity->causer->id ?? 'null'}}
                        </td>
                        <td class="">{{$activity->subject ? $activity->subject->first_name.' '.$activity->subject->last_name : "null"}}</td>
                        <td class="">{{$activity->subject->id ?? "null"}}</td>

                        <td>{{$activity->event}}</td>
                        <td>
                            {{$activity->created_at}}
                            {{--todo persion date--}}
                        </td>
                        <td class="w-50">
                            {{$activity->properties}}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <div class="">
                {{$activities->links()}}
            </div>
        </div>
    </div>


</main>

</body>
</html>
