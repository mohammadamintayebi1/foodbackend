@php use Illuminate\Support\Facades\Auth; @endphp


    <!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"
            defer>
    </script>

    <script src="https://cdn.tailwindcss.com"></script>


    {{-- <script
        type="text/javascript"
        src={{ asset('/assets/js/admin.js') }}
        defer
         ></script> --}}
    <meta name="_token" content="{{ csrf_token() }}">
    {{--    <script src="{{ asset('js/custom.js') }}" defer></script>--}}

    {{--    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>--}}

    <style>
        .back-tester {
            background-color: #d2d2d2;
        }
    </style>
    {{--    @vite(['/resources/js/app.js']) --}}

    <!-- Include Select2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" rel="stylesheet"/>
    <!-- Include jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Include Select2 JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js"></script>

    <style>
        .select2-container {
            width: 100%;
        }

        /* Custom styles for the Select2 dropdown */
        .select2-selection__arrow {
            height: 1.5em;
        }


        .select2.select2-container--default .select2-selection--single {
            background-color: #F9FAFB; /* bg-gray-50 */
            border: 1px solid #D1D5DB; /* border border-gray-300 */
            color: #111827; /* text-gray-900 */
            font-size: .875rem; /* text-sm */
            border-radius: .5rem; /* rounded-lg */
            padding-top: .625rem; /* p-2.5 */
            padding-bottom: .625rem; /* p-2.5 */
            height: auto;
        }

        .select2.select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 1.25rem /* Equivalent of leading-5 */;
            padding-left: .625rem; /* pl-2.5 */
            padding-right: .625rem; /* pr-2.5 */
        }

        .select2.select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 1.875rem; /* h-full */
        }

        .select2.select2-container--default .select2-dropdown {
            border-radius: .5rem; /* rounded-lg */
        }

        /* Focus states */
        .select2.select2-container--default .select2-selection--single:focus-within,
        .select2.select2-container--default.select2-container--focus .select2-selection--single {
            border-color: #3B82F6; /* border-blue-500 */
            --tw-ring-opacity: 1; /* focus:ring-opacity-50 */
            box-shadow: 0 0 0 2px rgba(59, 130, 246, var(--tw-ring-opacity)); /* focus:ring and focus:ring-blue-500 */
        }

        /* Optionally, add transitions for smooth focus effects */
        .select2 * {
            transition-property: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform;
            transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
            transition-duration: 150ms;
        }

    </style>

</head>


<body class="back-tester">
<main>
    <div>
        @if (session()->has('success'))
            <div class="alert alert-success mt-5 text-center">
                {{ session()->get('success') }}
                {{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button> --}}
            </div>
        @endif
        @if (session()->has('warning'))
            <div class="alert alert-warning  mt-5 text-center">
                {{ session()->get('warning') }}
                {{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button> --}}
            </div>
        @endif
        @if (session()->has('danger'))
            <div class="alert alert-danger  mt-5 text-center">
                {{ session()->get('danger') }}
                {{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button> --}}
            </div>
        @endif
    </div>

    <h1 class=" mt-20 text-center text-3xl font-bold ">
        nepp logs
    </h1>
    {{--    <div >Filter Logs</div>--}}
    <form
        class="w-25 mx-auto mt-20 border p-2"
        action="/logs" method="get">
        <input type="hidden" name="key" value="rL05Q1vuw26I3988gZNyz2Q5v7JBNlcAPa8Ln0Tgu7SAIY2fEq">
        <p class="font-bold text-center mb-2">Filter: </p>
        <div class="flex flex-row space-x-4  items-center mb-3">


            <div class="flex-1">
                <select
                    name="log_name"
                    class="bg-gray-50 border border-gray-300 text-gray-900  rounded-lg focus:ring-blue-500 focus:border-blue-500  w-full p-2.5"
                >
                    <option value="all" @selected($logNameInput === 'all')>Category</option>
                    @foreach($logNames as $logName)
                        <option
                            value="{{$logName}}" {{ $logNameInput == $logName ? 'selected' : '' }} >{{ucfirst($logName)}} </option>
                    @endforeach

                </select>

            </div>
            <div class="flex-1 ">
                {{--            <label for="countries" class="block mb-2 text-sm font-medium text-white">Type</label>--}}
                <select id=""
                        name="event"
                        class="focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500 rounded-lg">

                    <option value="all">Event</option>
                    @foreach($events as $event)
                        <option
                            value="{{$event}}" {{$eventInput == $event ? 'selected' : '' }}>{{ucfirst($event)}}</option>
                    @endforeach
                </select>

            </div>

            <div class="flex-1">
                <select
                    name="role"
                    class="bg-gray-50 border border-gray-300 text-gray-900  rounded-lg focus:ring-blue-500 focus:border-blue-500  w-full p-2.5"
                >
                    <option value="all">Role</option>
                    @foreach($roles as $index => $role)
                        <option
                            value="{{$role->id}}" {{ $roleInput == $role->id ? 'selected' : '' }} >{{ $role->pName}} </option>
                    @endforeach

                </select>

            </div>


        </div>

        <div class="mb-3">
            <select
                name="causer_id"
                id="searchableSelect"
                style=""
                class="select2">
                <option value="all">Causer</option>
                @foreach ($causers as $causer)
                    <option
                        value="{{ $causer->id }}"
                        {{ $causerIdInput == $causer->id ? 'selected' : '' }}
                    >
                        {{ $causer->first_name.' '.$causer->last_name }}</option>
                @endforeach
            </select>

        </div>


        <div class="flex flex-row justify-center space-x-4 w-50 mx-auto">
            <button type="submit" class="flex-1 bg-green-300 py-2 px-6 rounded">Search
            </button>

            <a href="/logs?key=rL05Q1vuw26I3988gZNyz2Q5v7JBNlcAPa8Ln0Tgu7SAIY2fEq"
               class="flex-1 text-center bg-blue-300 px-6 py-2 rounded"
            >Clear
            </a>
        </div>
    </form>
    <div class="relative overflow-x-auto px-10 mt-20">
        <table class="w-full text-sm  text-gray-500 items-center">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 border-b border-gray-600">
            <tr>
                <th scope="col" class="px-6 py-3">
                    ID
                </th>
                <th scope="col" class="px-6 py-3">
                    Category
                </th>
                <th scope="col" class="px-6 py-3">
                    Event
                </th>
                <th scope="col" class="px-6 py-3">
                    Causer Name
                </th>
                <th scope="col" class="px-6 py-3">
                    Causer ID
                </th>
                <th scope="col" class="px-6 py-3">
                    Causer Role
                </th>
                <th scope="col" class="px-6 py-3">
                    Subject
                </th>
                <th scope="col" class="px-6 py-3">
                    Subject ID
                </th>

                <th scope="col" class="px-6 py-3">
                    Date & Time
                </th>
                <th scope="col" class="px-6 py-3">
                    Details
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($activities as $activity)
                <tr class="bg-white border-b border-gray-400 text-black">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{$activity->id}}
                    </th>
                    <td class="px-6 py-4">
                        {{$activity->log_name}}
                    </td>
                    <td class="px-6 py-4">{{ucfirst($activity->event)}}</td>
                    <td class="px-6 py-4">
                        {{$activity->causer ? $activity->causer->first_name.' '.$activity->causer->last_name :  'null'}}
                    </td>
                    <td class="px-6 py-4">
                        {{$activity->causer ? $activity->causer->id :  'null'}}
                    </td>
                    <td class="px-6 py-4">
                        {{$activity->causer ? $activity->roleName :  'null'}}
                    </td>
                    <td class="px-6 py-4">{{$activity->subject ? class_basename($activity->subject) : "null"}}</td>
                    <td class="px-6 py-4">{{$activity->subject->id ?? "null"}}</td>

                    <td class="px-6 py-4">{{$activity->created_at}}</td>
                    <td class="px-6 py-4">
                        <a
                            href="/logs/{{$activity->id}}?key=rL05Q1vuw26I3988gZNyz2Q5v7JBNlcAPa8Ln0Tgu7SAIY2fEq"
                            class="px-4 py-2 bg-blue-400 rounded shadow-md">View</a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
        <div class="mt-5">
            {{$activities->appends(['key' => 'rL05Q1vuw26I3988gZNyz2Q5v7JBNlcAPa8Ln0Tgu7SAIY2fEq'])->links()}}
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#searchableSelect').select2({
                // theme: 'classic', // or 'default' or 'bootstrap' or other available themes
                width: '100%',

            });
        });
    </script>

</main>

</body>
</html>
