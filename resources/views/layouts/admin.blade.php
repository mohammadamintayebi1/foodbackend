@php use Illuminate\Support\Facades\Auth; @endphp


    <!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"
            defer>
    </script>

    <script src="https://cdn.tailwindcss.com"></script>





    {{-- <script
        type="text/javascript"
        src={{ asset('/assets/js/admin.js') }}
        defer
         ></script> --}}
    <meta name="_token" content="{{ csrf_token() }}">
    {{--    <script src="{{ asset('js/custom.js') }}" defer></script>--}}

    {{--    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>--}}

    <style>
        .back-tester {
            background-color: #d2d2d2;
        }
    </style>
    {{--    @vite(['/resources/js/app.js']) --}}




    <!-- Include Select2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" rel="stylesheet" />
    <!-- Include jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Include Select2 JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js"></script>





</head>

<body class="back-tester">
<main>
    @if (session()->has('success'))
        <div class="alert alert-success mt-5 text-center">
            {{ session()->get('success') }}
            {{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button> --}}
        </div>
    @endif
    @if (session()->has('warning'))
        <div class="alert alert-warning  mt-5 text-center">
            {{ session()->get('warning') }}
            {{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button> --}}
        </div>
    @endif
    @if (session()->has('danger'))
        <div class="alert alert-danger  mt-5 text-center">
            {{ session()->get('danger') }}
            {{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button> --}}
        </div>
    @endif



    {{ $slot }}
</main>

</body>
</html>
