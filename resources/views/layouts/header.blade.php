<!DOCTYPE html>

<html lang="fa">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>داشبورد | راهه کار</title>
    <meta name="description" content="Adminice is a Web App and Admin Dashboard Template built with Bootstrap 4" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/') }}/assets/img/apple-touch-icon.png?v=1" />
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/') }}/assets/img/favicon-32x32.png?v=1" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/') }}/assets/img/favicon-16x16.png?v=1" />
    <!-- Stylesheet -->
    <!-- Font Iran licence -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/fontiran.css" />

    <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/css/base/bootstrap.min.css" />
    <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/css/base/seenboard-1.0.css?v=2.0" />
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/owl-carousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/owl-carousel/owl.theme.min.css" />
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/animate/animate.min.css">
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
</head>

<body id="page-top">
    <!-- Begin Preloader --
    <div id="preloader">
        <div class="canvas">
            <img src="{{ url('/') }}/assets/img/rahekar-FA.svg" alt="logo" class="loader-logo" />
            <div class="spinner"></div>
        </div>
    </div>
    <!-- End Preloader -->
    <div class="page">
        <!-- Begin Header -->
        <header class="header">
            <nav class="navbar fixed-top">
                <!-- Begin Search Box-->
                <div class="search-box">
                    <button class="dismiss"><i class="ion-close-round"></i></button>
                    <form id="searchForm" action="#" role="search">
                        <input type="search" placeholder="جستجو کنید ..." class="form-control" />
                    </form>
                </div>
                <!-- End Search Box-->
                <!-- Begin Topbar -->
                <div class="navbar-holder d-flex align-items-center align-middle justify-content-between">
                    <!-- Begin Logo -->
                    <div class="navbar-header">
                        {{-- <a href="/" class="navbar-brand">
                            <div class="brand-image brand-big">
                                <img src="{{ url('/') }}/assets/img/rahekar-FA.svg" alt="logo" class="logo-big" />
                            </div>
                            <div class="brand-image brand-small">
                                <img src="{{ url('/') }}/assets/img/rahekar-FA.svg" alt="logo" class="logo-small" />
                            </div>
                        </a> --}}
                        <!-- Toggle Button -->
                        <a id="toggle-btn" href="#" class="menu-btn active">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                        <!-- End Toggle -->
                    </div>
                    <!-- End Logo -->
                    <!-- Begin Navbar Menu -->
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center pull-right">
                        <!-- End Notifications -->
                        <!-- User -->
                        <li class="nav-item dropdown">
                            <a id="user" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><img src="{{ url('/upload/avatar/' . auth()->user()->id) }}" alt="..." class="avatar rounded-circle avatar1" /></a>
                            <ul aria-labelledby="user" class="user-size dropdown-menu">
                                <li>
                                    <br />
                                    <a href="{{ route('editProfile') }}" class="dropdown-item">
                                        ویرایش پروفایل
                                    </a>
                                </li>
                                <li class="separator"></li>
                                <li>
                                    <a href="{{ route('resetPassword') }}" class="dropdown-item no-padding-top">
                                        تغییر رمز عبور </a>
                                </li>
                                <li class="separator"></li>
                                <li>
                                    <a href="{{ route('logout') }}" class="dropdown-item no-padding-top">
                                        خروج </a><br>
                                </li>
                            </ul>
                        </li>
                        <!-- End User -->
                        <!-- Begin Quick Actions -->

                        <!-- End Quick Actions -->
                    </ul>
                    <!-- End Navbar Menu -->
                </div>
                <!-- End Topbar -->
            </nav>
        </header>
        <!-- End Header -->
        <!-- Begin Page Content -->
        <div class="page-content d-flex align-items-stretch">
            <div class="default-sidebar">
                <!-- Begin Side Navbar -->
                <nav class="side-navbar box-scroll sidebar-scroll">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-0 vavatar">
                        <div class="widget widget-13 pb-2 pt-2">
                            <div class="widget-body p-0">
                                <div class="mt-5">
                                    <img src="{{ url('/upload/avatar/' . auth()->user()->id) }}" alt="..." style="width: 120px;" class="avatar rounded-circle d-block mx-auto avatar2">
                                </div>
                                <h3 class="text-center mt-3 mb-1">
                                    {{ auth()->user()->first_name . ' ' . auth()->user()->last_name }}
                                </h3>
                                <p class="text-center">{{ auth()->user()->email }}</p>
                            </div>
                        </div>
                    </div>
                    <!-- Begin Main Navigation -->
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{ route('dashboard') }}"><i class="la la-columns"></i><span>داشبورد</span></a>
                        </li>
                        {{-- <li>
                            <a href="{{ route('profile') }}"><i class="la la-user"></i><span>پروفایل</span></a>
                        </li> --}}
                        @can('user.list')
                            <li>
                                <a href="{{ route('allUsers') }}"><i class="la la-users"></i><span>مدیریت کاربران
                                    </span></a>
                            </li>
                        @endcan
                        @can('resume.list')
                            <li>
                                <a href="{{ route('allResume') }}"><i class="la la-folder-open-o"></i><span>مدیریت رزومه ها
                                    </span></a>
                            </li>
                        @endcan
                        @can('resume.insert')
                            <li>
                                <a href="{{ route('profile') }}"><i class="la la-file-text"></i><span>تکمیل
                                        رزومه</span></a>
                            </li>
                        @endcan
                        <li>
                            <a href="{{ route('test') }}"><i class="la la-puzzle-piece"></i><span>تست شخصیت
                                    شناسی</span></a>
                        </li>
                        <li>
                            <a href="{{ route('learning') }}"><i class="la la-graduation-cap"></i><span>آموزش</span></a>
                        </li>
                        <li>
                            <a href="{{ route('resetPassword') }}"><i class="la la-key"></i><span>تغییر رمز
                                    عبور</span></a>
                        </li>
                        <li>
                            <a href="{{ route('editProfile') }}"><i class="la la-user"></i><span>ویرایش
                                    پروفایل</span></a>
                        </li>
                        <li>
                            <a href="{{ route('allTicket') }}"><i class="la la-ticket"></i><span>تیکت
                                    پشتیبانی</span></a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"><i class="la la-sign-out"></i><span>خروج</span></a>
                        </li>
                    </ul>
                    <!-- End Main Navigation -->
                </nav>
                <!-- End Side Navbar -->
            </div>
            <!-- End Left Sidebar -->
            <div class="content-inner">
