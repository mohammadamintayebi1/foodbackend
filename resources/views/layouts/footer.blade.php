<!-- Begin Page Footer-->
<footer class="main-footer">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex align-items-center justify-content-xl-start justify-content-lg-start justify-content-md-start justify-content-center">
            <p class="text-gradient-02">
                طراحی شده | دپارتمان انفورماتیک راهه‌کار
            </p>
        </div>
        {{-- <div
            class="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex align-items-center justify-content-xl-end justify-content-lg-end justify-content-md-end justify-content-center">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="documentation.html">مستندات</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="changelog.html">آپدیت ها</a>
                </li>
            </ul>
        </div> --}}
    </div>
</footer>
<!-- End Page Footer -->
<a href="#" class="go-top"><i class="la la-arrow-up"></i></a>
<!-- Offcanvas Sidebar -->
</div>
<!-- End Content -->
</div>
<!-- End Page Content -->
</div>
<!-- Begin Vendor Js -->
<script src="{{ url('/') }}/assets/vendors/js/base/jquery.min.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/base/core.min.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/base/jquery.validate.min.js"></script>
<!-- End Vendor Js -->
<!-- Begin Page Vendor Js -->
<script src="{{ url('/') }}/assets/vendors/js/nicescroll/nicescroll.min.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/chart/chart.min.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/progress/circle-progress.min.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/calendar/moment.min.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/calendar/fullcalendar.min.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/calendar/locale/fa.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/owl-carousel/owl.carousel.min.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/noty/noty.min.js"></script>
<script src="{{ url('/') }}/assets/vendors/js/app/app.js?v=1.1"></script>
<script src="{{ url('/') }}/assets/vendors/js/bootstrap-wizard/bootstrap.wizard.min.js"></script>
<!-- End Page Vendor Js -->
<!-- Begin Page Snippets -->
<script src="{{ url('/') }}/assets/js/dashboard/db-default.js"></script>
<script src="{{ url('/') }}/assets/js/components/wizard/form-wizard.min.js"></script>
<!-- End Page Snippets -->
</body>

</html>
